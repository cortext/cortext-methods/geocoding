import pytest

from pathlib import Path
from string import Template
from shutil import copyfile

from cortextlib import Job

from geocoding.stages import gaia_geocoding


@pytest.fixture
def corpus_name():
  return "small_corpus"


@pytest.fixture
def cortext_corpus_filenames(corpus_name):
  return dict(db_file=corpus_name + ".db", descriptor_file=corpus_name + ".yaml")


@pytest.fixture
def stub_params():
  return dict(
    job_id="xx",
    label="xx",
    project_id="xx",
    user_id="xx",
    cortext_project_id="xx",
    cortext_analysis_id="xx",
    cortext_callback_json="xx",
    cortext_username="xx",
    script_path="xx",
    script_id="xx",
    corpus_id="xx",
    created_at="xx",
  )


@pytest.fixture
def prepared_params(
  tmp_path, request, stub_params, corpus_name, cortext_corpus_filenames
):
  resources_path = request.path.parent / "resources"

  # copy corpus file
  db_file = cortext_corpus_filenames["db_file"]
  descriptor_file = cortext_corpus_filenames["descriptor_file"]
  copyfile(resources_path / db_file, tmp_path / db_file)
  copyfile(resources_path / descriptor_file, tmp_path / descriptor_file)

  params_template_path = resources_path / "param_template.yml"
  paramfile_raw = Path(params_template_path).read_text()
  param_template = Template(paramfile_raw)
  instance_params = (
    stub_params
    | dict(  # https://docs.python.org/3/library/stdtypes.html#dict.update
      address_field="address",
      source="wos",
      threshold_layer="country",
      mode="Filtering non geographical information",
      advanced_settings_geocoding="yes",
      threshold_confidence="0.0",
      corpus_file=tmp_path / db_file,
      corpus_name=corpus_name,
      result_path=tmp_path,
    )
  )
  rendered_params = param_template.substitute(instance_params)
  params_path = tmp_path / "param.yml"
  params_path.write_text(rendered_params, encoding="utf-8")
  return params_path


def test_command(tmp_path, prepared_params, cortext_corpus_filenames):
  # arrange
  job = Job(ctm_params=str(prepared_params))
  expected_script_path = gaia_geocoding.script_path
  input_file = tmp_path / "a_file.csv"
  output_file = tmp_path / "output.csv"

  expected_command = [
    "perl",
    str(expected_script_path),
    "--benchmark_all",
    "off",
    "--stats_print",
    "off",
    "--verbose",
    "--progress_bar",
    "off",
    "--progress_file",
    str(tmp_path / "progress.yaml"),
    "--log_file",
    str(tmp_path / ".user.log"),
    "--database",
    str(tmp_path / cortext_corpus_filenames["db_file"]),
    "--geocoding_source",
    "wos",
    "--geocoding_mode",
    "norm-code",
    "--csv_in_address",
    "address",
    "--threshold_layer",
    "country",
    "--threshold_confidence",
    "0.0",
    "--output",
    str(output_file),
    "--columns",
    "label;longitude;latitude;confidence;accuracy;layer;city;region;country;iso2;iso3",
    str(input_file),
  ]

  # act
  command = gaia_geocoding.build_gaia_command(
    progress_file=job.progress._path,
    log_file=job.dir / ".user.log",
    db_file=job.db.path,
    source=job.uparams["source"],
    mode=job.uparams["mode"],
    address_field="ignored",
    threshold_layer=job.uparams["threshold_layer"],
    threshold_confidence=job.uparams["threshold_confidence"],
    output_filepath=output_file,
    input_filepath=input_file,
    job_dir=job.dir,
  )

  # assert
  assert command == expected_command
