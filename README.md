# Cortext Geocoding

Cortext Geocoding is a method in [Cortext Manager](https://docs.cortext.net/) that allows enriching a dataset with the geographical coordinates corresponding to the toponyms contained in one of its fields. Since toponyms extracted from a textual corpus can vary in quality and scope, the method provides several parameters to fit different use cases, such as whether to give high priority to a specific scale (e.g. regional or city scale). For more usage details, see the method's [user documentation](https://docs.cortext.net/geocoding-addresses/).

# Description

Cortext Geocoding is a python project that uses [cortextlib](https://gitlab.com/cortext/cortext-methods/cortextlib) to interface with the cortext infrastructure, and uses the independent [gaia worker perl script](./src/external/gaia) to perform the actual bulk-geocoding of addresses contained in cortext datasets.

The method is organized in three stages: deduplicating identifiers, calling the gaia script to do geocoding and assigning a score to the results.

The deduplication stage is highly coupled with cortext manager's data model, if a a non-unique triplet of (id, rank, parserank) is found in the field containing toponyms, a copy of said field with unique (id, rank, parserank) triples is created and this copy is used for the later stages.

The gaia script performs various preprocessing steps according to the parameters, makes use of [libpostal](https://github.com/openvenues/libpostal) to try to extract structured information from the toponyms provided, and uses [cortext's fork](https://gitlab.com/cortext/cortext-geocoding-service/) of the [Pelias geocoding engine](https://github.com/pelias/pelias) to perform geocoding and add the coordinates and other structured geographical information to the dataset.

The scoring stage uses weighted edit distance metrics between the input toponyms and several attributes in the output information to assign a confidence score to the results.

# Dependencies

- Python >= 3.9.2
- Perl >= 5.32
- [Libpostal v1.1](https://github.com/openvenues/libpostal)
- Python dependencies are in [pyproject.toml](./pyproject.toml)
- Perl dependencies are in [src/external/gaia/cpanfile](./scripts/gaia/cpanfile)

# Build

- Python dependencies and this package can be installed in editable mode with:

```
pip3 install -e .
```

- Libpostal can be installed following [libpostal's installation steps](https://github.com/openvenues/libpostal?tab=readme-ov-file#installation-maclinux).

- Perl dependencies can be installed with with [cpanm](https://metacpan.org/dist/App-cpanminus/view/bin/cpanm):

```
cpanm --notest --installdeps ./src/external/gaia
```

## With guix:

Make sure you are using the [Cortext Guix channel](https://gitlab.com/cortext/cortext-guix-channel).

```
guix time-machine -C channels.scm -- shell
```

Will spawn a shell with all dependencies on installed. Geocoding can then be installed locally with `pip3 install -e .`.

# Test

```
pytest tests
```

# Configuration

The gaia perl script has a [configuration file](./src/external/gaia/config.ini) where several options can be specified. Make sure to set the address and port of the geocoding engine and the placeholder subservice.

# Use

Cortext geocoding, as other cortext methods, expects the path to a cortext parameter file as its only argument:

```
python3 -m geocoding /path/to/param.yml
```

# License

This software is available as free and open source under the terms of the [GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html#license-text). 
