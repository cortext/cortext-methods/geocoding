(list (channel
        (name 'cortext)
        (url "https://gitlab.com/cortext/cortext-guix-channel.git")
        (branch "main")
        (commit
          "3b869e431a6fa0ea40611a3ea21e179aa6fef9bd"))
      (channel
        (name 'guix)
        (url "https://git.savannah.gnu.org/git/guix.git")
        (branch "master")
        (commit
          "4003c60abf7a6e59e47cc2deb9eef2f104ebb994")
        (introduction
          (make-channel-introduction
            "9edb3f66fd807b096b48283debdcddccfea34bad"
            (openpgp-fingerprint
              "BBB0 2DDF 2CEA F6A8 0D1D  E643 A2A0 6DF2 A33A 54FA")))))
