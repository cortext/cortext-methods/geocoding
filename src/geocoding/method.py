# cortext_geocoding - Cortext Geocoding Method
#
# Author (s):
# * Luis Daniel Medina Zuluaga <luis-daniel.medina-zuluaga@univ-eiffel.fr>
#
# License:
# [GNU-GPLv3+](https://www.gnu.org/licenses/gpl-3.0.html)
#
# Reference repository:
# <https://gitlab.com/cortext/cortext-methods/geocoding>
#
# Contributions are welcome, get in touch with the author(s).

from cortextlib import CortextMethod
from contextlib import closing
import csv


class GeocodingMethod(CortextMethod):
  def __init__(self, job):
    super().__init__(job)

  def __exit__(self, exc_type, exc_value, traceback):
    super().__exit__(exc_type, exc_value, traceback)

  def main(self, address_field, source, threshold_layer, mode, advanced_settings_geocoding, threshold_confidence):
    from .stages import geouniqueid, gaia_geocoding, geoconfidence

    job = self.job
    logger = job.logger
    data = job.data

    # dedup ids

    was_dedupped, input_table_name = geouniqueid.dedup_geo_table(
      data.db_con, address_field, logger
    )

    if was_dedupped:
      job.db._metadata_update_root(
        updater=lambda descriptor: add_table_to_descriptor(descriptor, input_table_name)
      )

    logger.debug(f"the geocoding script will use '{input_table_name}' as input")

    # call gaia perl script

    # the gaia perl script needs a csv dump of the address table with the "data" column renamed as "address"
    address_filepath = job.dir / ".address.csv"

    dump_table_to_csv(
      data,
      input_table_name,
      address_filepath,
      delimiter=";",
      quotechar='"',
      col_mapping=dict(data="address"),
    )

    new_tables = gaia_geocoding.run_geocoding(
      progress_file=job.progress._path,
      log_file=job.dir / ".user.log",
      db_file=job.db.path,
      db_con=data.db_con,
      source=source,
      mode=mode,
      address_field=input_table_name,
      threshold_layer=threshold_layer,
      threshold_confidence=threshold_confidence,
      input_filepath=address_filepath,
      job_dir=job.dir,
      logger=logger,
    )

    for table_name in new_tables:
      job.db._metadata_update_root(
        updater=lambda descriptor: add_table_to_descriptor(descriptor, table_name)
      )

    # confidence score

    confidence_score_table = geoconfidence.rate_geocoding_results(data, job.dir, logger)
    job.db._metadata_update_root(
      updater=lambda descriptor: add_table_to_descriptor(
        descriptor, confidence_score_table
      )
    )

    # fin

def dump_table_to_csv(
  data, tablename, filepath, delimiter, quotechar, col_mapping=dict()
):
  with closing(data.db_con.cursor()) as cur:
    with open(filepath, "w") as address_file:
      input_table = cur.execute(data.raw_table_sql.format(table_name=tablename))
      csv_writer = csv.writer(address_file, delimiter=delimiter, quotechar=quotechar)
      columns = [col_mapping.get(d[0], d[0]) for d in input_table.description]
      csv_writer.writerow(columns)
      for db_row in input_table:
        csv_writer.writerow(db_row)


def add_table_to_descriptor(descriptor, new_table, is_text_table=False):
  def add_to_fieldlist(descriptor, key, new_element):
    fieldlist_set = set(descriptor.get(key, []))
    fieldlist_set.add(new_element)
    descriptor[key] = list(fieldlist_set)

  add_to_fieldlist(descriptor, "tablenames", new_table)
  add_to_fieldlist(descriptor, "totaltables", new_table)
  add_to_fieldlist(descriptor, "alltables", new_table)

  if is_text_table:
    add_to_fieldlist(descriptor, "textual_fields", new_table)
