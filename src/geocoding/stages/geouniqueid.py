# cortext_geocoding - Cortext Geocoding Method
#
# Author (s):
# * Luis Daniel Medina Zuluaga <luis-daniel.medina-zuluaga@univ-eiffel.fr>
#
# License:
# [GNU-GPLv3+](https://www.gnu.org/licenses/gpl-3.0.html)
#
# Reference repository:
# <https://gitlab.com/cortext/cortext-methods/geocoding>
#
# Contributions are welcome, get in touch with the author(s).

import sqlite3

def dedup_geo_table(db_con, tablename, logger):
  cur = db_con.cursor()
  cur.execute(
    f"select id, rank, parserank, count(1) as cnt from {tablename} group by id, rank, parserank having cnt > 1 limit 1"
  )
  if cur.fetchone():
    new_table_name = remake_input_table(db_con, tablename, logger)
    result = (True, new_table_name)
  else:
    logger.debug(f"no duplicate ids found in {tablename}")
    result = (False, tablename)
  cur.close()
  return result


def remake_input_table(db_con, tablename, logger):
  new_table_name = "geo__" + tablename
  logger.debug(
    f"duplicate ids found in {tablename}. copying {tablename} to {new_table_name}"
  )
  db_con.execute(f"drop table if exists {new_table_name}")
  db_con.execute(
    f"create table {new_table_name} (file TEXT, id INT, rank INT, parserank TEXT, data TEXT)"
  )
  ordered_id_query = f"select file, id, rank, parserank, data from {tablename} order by id, rank, parserank"
  insert_query = f"insert into {new_table_name}(file, id, rank, parserank, data) values(?, ?, ?, ?, ?)"
  current_id = (0, 0, 0)
  rank = 0
  cur = db_con.cursor()
  cur.row_factory = sqlite3.Row
  for row in cur.execute(ordered_id_query):
    row_id = (row["id"], row["rank"], row["parserank"])
    if row_id != current_id:
      current_id = row_id
    rank += 1
    db_con.execute(
      insert_query,
      (
        row["file"],
        current_id[0],
        rank,
        current_id[2],
        row["data"],
      ),
    )
  db_con.commit()
  cur.close()
  logger.debug(f"finished building {new_table_name} with dedupped ids")
  return new_table_name
