# cortext_geocoding - Cortext Geocoding Method
#
# Author (s):
# * Luis Daniel Medina Zuluaga <luis-daniel.medina-zuluaga@univ-eiffel.fr>
#
# License:
# [GNU-GPLv3+](https://www.gnu.org/licenses/gpl-3.0.html)
#
# Reference repository:
# <https://gitlab.com/cortext/cortext-methods/geocoding>
#
# Contributions are welcome, get in touch with the author(s).


import subprocess
from collections import OrderedDict

from ..utils import get_scripts_dir

script_path = get_scripts_dir() / "gaia" / "gaia-worker.pl"

output_filename = "geocoded.csv"

required_columns = (
  "label",
  "longitude",
  "latitude",
  "confidence",
  "accuracy",
  "layer",
  "city",
  "region",
  "country",
  "iso2",
  "iso3",
)

columns_option = ";".join(required_columns)

mode_map = {
  "Filtering non geographical information": "norm-code",
  "Priority to the street level": "macro-micro",
  "Priority to the city level": "macro-meso",
  "No customization": "vanilla",
}


def build_gaia_command(
  progress_file,
  log_file,
  db_file,
  source,
  mode,
  address_field,
  threshold_layer,
  threshold_confidence,
  output_filepath,
  input_filepath,
  job_dir,
):
  command = OrderedDict()

  command["command"] = ["perl", str(script_path)]

  command["options"] = []
  command["options"].extend(["--benchmark_all", "off"])
  command["options"].extend(["--stats_print", "off"])
  command["options"].extend(["--verbose"])

  command["progress"] = []
  command["progress"].extend(["--progress_bar", "off"])
  command["progress"].extend(["--progress_file", str(progress_file)])

  command["params"] = []
  command["params"].extend(["--log_file", str(log_file)])
  command["params"].extend(["--database", str(db_file)])
  command["params"].extend(["--geocoding_source", str(source)])
  command["params"].extend(["--geocoding_mode", mode_map[mode]])
  # gaia script wants the column in the dumped csv file, which we historically hardcoded as "address"
  command["params"].extend(["--csv_in_address", "address"])
  command["params"].extend(["--threshold_layer", threshold_layer])
  command["params"].extend(["--threshold_confidence", str(threshold_confidence)])
  command["params"].extend(["--output", str(output_filepath)])
  command["params"].extend(["--columns", columns_option])

  command["args"] = [str(input_filepath)]

  command_array = sum(command.values(), [])

  return command_array


def create_derived_tables(db_con, logger):
  table_column_map = dict(
    geo_region="region",
    geo_quality="layer",
    geo_country="country",
    geo_city='case when city <> "" then city || ", " || lower(iso3) else "" end',
    geo_longitude_latitude='(longitude || "|" || latitude)',
  )
  drop_queries = [f"drop table if exists {table}" for table in table_column_map.keys()]
  create_queries = [
    f"create table if not exists {table} as select file, id, rank, parserank, {column_expr} as data from geo_address"
    for table, column_expr in table_column_map.items()
  ]
  for query in drop_queries + create_queries:
    db_con.execute(query)
  return list(table_column_map.keys())


def run_geocoding(
  progress_file,
  log_file,
  db_file,
  db_con,
  source,
  mode,
  address_field,
  threshold_layer,
  threshold_confidence,
  input_filepath,
  job_dir,
  logger,
):
  output_filepath = job_dir / output_filename

  command = build_gaia_command(
    progress_file,
    log_file,
    db_file,
    source,
    mode,
    address_field,
    threshold_layer,
    threshold_confidence,
    output_filepath,
    input_filepath,
    job_dir,
  )

  cmdstring = " ".join(command)

  logger.debug(f"invoking command {cmdstring}")

  debuglog_path = job_dir / ".debug.log"
  with debuglog_path.open("a") as dlog_f:
    child_process = subprocess.run(command, stdout=dlog_f, stderr=subprocess.STDOUT)

  try:
    child_process.check_returncode()
    logger.debug("command script finished successfully.")
    new_tables = create_derived_tables(db_con, logger)
    return new_tables
  except subprocess.CalledProcessError as exc:
    logger.debug("command script returned nonzero code.")
    raise exc
