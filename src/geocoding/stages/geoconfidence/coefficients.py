# cortext_geocoding - Cortext Geocoding Method
#
# Author (s):
# * Luis Daniel Medina Zuluaga <luis-daniel.medina-zuluaga@univ-eiffel.fr>
#
# License:
# [GNU-GPLv3+](https://www.gnu.org/licenses/gpl-3.0.html)
#
# Reference repository:
# <https://gitlab.com/cortext/cortext-methods/geocoding>
#
# Contributions are welcome, get in touch with the author(s).

layers = {
  "postalcode": {"score": 1},
  "disputed": {"score": 0.5},
  "dependency": {"score": 0.5},
  "neighbourhood": {"score": 0.5},
  "borough": {"score": 0.5},
  "locality": {"score": 0.5},
  "localadmin": {"score": 0.5},
  "county": {"score": 0.5},
  "macrocounty": {"score": 0.5},
  "region": {"score": 0.4},
  "macroregion": {"score": 0.4},
  "country": {"score": 0.3},
  "macrohood": {"score": 0.1},
  "street": {"score": 0.1},
  "venue": {"score": 0.1},
  "address": {"score": 0.1},
}

scores = {"weights": {"layer": 0.3, "similarity": 0.7}, "similarity_threshold": 50}
