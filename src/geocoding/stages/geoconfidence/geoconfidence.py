# cortext_geocoding - Cortext Geocoding Method
#
# Author (s):
# * Luis Daniel Medina Zuluaga <luis-daniel.medina-zuluaga@univ-eiffel.fr>
#
# License:
# [GNU-GPLv3+](https://www.gnu.org/licenses/gpl-3.0.html)
#
# Reference repository:
# <https://gitlab.com/cortext/cortext-methods/geocoding>
#
# Contributions are welcome, get in touch with the author(s).

from nltk.util import ngrams
from rapidfuzz import fuzz
import pandas as pd

from . import coefficients


def rate_geocoding_results(data, job_dir, logger):
  # read non-standard geo_address table

  geocoded_df = pd.read_sql_query(
    "select file, id, rank, parserank, address, label, layer from geo_address",
    data.db_con,
  )

  # calculate scores

  geocoded_df["confidence_score"] = geocoded_df.apply(
    lambda row: round(score(row["label"], row["address"], row["layer"]), 2), axis=1
  )

  # add table to corpus db

  score_tablename = "geo_score"
  geocoded_df["data"] = geocoded_df["confidence_score"]
  geocoded_df = geocoded_df[["file", "id", "rank", "parserank", "data"]]
  geocoded_df.to_sql(
    con=data.db_con,
    name=score_tablename,
    if_exists="replace",
    index=False,
    chunksize=2000,
  )

  # write stub file for user interface

  with open(job_dir / "result.geoedit", "w+") as file:
    file.write(
      "This file is intentionally left blank. Click the 'eye' button in the manager to refine your geocoding results."
    )

  return score_tablename


def score(short_s, long_s, layer):
  weights = coefficients.scores["weights"]
  scores = [similarity_score(short_s, long_s), (layer_score(layer) * 100)]
  if scores[0] < coefficients.scores["similarity_threshold"]:
    return weights["similarity"] * scores[0]
  return weights["similarity"] * scores[0] + weights["layer"] * scores[1]


def similarity_score(short_s, long_s):
  long_s = clean(long_s)
  tokens = short_s.split(",")
  if len(tokens) == 1:
    tokens = short_s.split(" ")
  score = 0
  for search_term in tokens:
    search_nwords = len(search_term.split())
    ngram_length = search_nwords + int(0.2 * search_nwords)
    long_split = long_s.split()
    if ngram_length > len(long_split):
      ngram_length = len(long_split)
    max_similarity = 0
    for long_ngram in ngrams(long_split, ngram_length):
      search_slide = " ".join(long_ngram)
      search_term = clean(search_term)
      max_similarity = max(max_similarity, string_similarity(search_term, search_slide))
    score += max_similarity
  return score / len(tokens)


def string_similarity(first, second):
  return fuzz.token_sort_ratio(first, second)


def layer_score(layer):
  try:
    return coefficients.layers[layer]["score"]
  except KeyError:
    return 0


def clean(string):
  string = string.replace("-", " ")
  string = string.replace(",", "")
  string = string.replace(".", "")
  string = string.strip()
  return string
