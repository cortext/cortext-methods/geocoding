package Fingerprint;
# =============================================================================
# Fingerprint Functions
# =============================================================================
# mfh_fingerprint          : Most frequent kChars
# normalized_fingerprint   : Normalized fingerprint with transliteration
# ngrams_fingerprint       : N-gram fingerprint
# =============================================================================
# Copyright 2016 by Cristian Martinez <me|at|martinec.org>
# License: GNU General Public License Version 3 or later
# =============================================================================
use parent 'Exporter';
# =============================================================================
use strict;                               # Pragma to restrict unsafe constructs
use warnings;                             # Pragma to control optional warnings
# =============================================================================
use Text::Unaccent::PurePerl qw(unac_string);
use Text::Unidecode;
# =============================================================================
use Text::Ngram qw(ngram_counts);         # Count Ngrams
# =============================================================================
use Data::Dumper;                         # Stringified perl data structures
# =============================================================================
use utf8;                                 # Enables to type utf8 in program code
use open IN  => ':encoding(utf8)';        # Input stream utf8
use open OUT => ':encoding(utf8)';        # Output stream utf8
use open        ':std';                   # STDIN, STDOUT, STDERR utf8
# =============================================================================
# Fingerprint  function : reverse
# =============================================================================
my $func_reverse = sub {
  my $str = reverse(shift);
  return $str;
};

# =============================================================================
# Fingerprint  function : lowercase
# =============================================================================
my $func_lowercase = sub {
  my $str = lc(shift);
  return $str;
};

# =============================================================================
# Fingerprint  function : uppercase
# =============================================================================
my $func_uppercase = sub {
  my $str = uc(shift);
  return $str;
};

# =============================================================================
# Fingerprint  function : capitalize
# =============================================================================
my $func_capitalize = sub {
  my $str = shift;
  $str =~ s/([\w']+)/\u\L$1/g;
  return $str;
};

# =============================================================================
# Fingerprint  function : unaccent
# =============================================================================
my $func_unaccent = sub {
  my $str = unac_string(shift);
  return $str;
};

# =============================================================================
# Fingerprint  function : transliterate
# =============================================================================
my $func_transliterate = sub {
  my $str = unidecode(shift);
  return $str;
};

# =============================================================================
# Fingerprint  function : concatenate
# =============================================================================
my $func_concatenate = sub {
  my $lhs = shift;
  my $rhs = shift;
  return $lhs . $rhs;
};

# =============================================================================
# Fingerprint  function : trim
# =============================================================================
my $func_trim = sub {
 my $str = shift;
 $str =~ s/^\s+//g;  # ltrim
 $str =~ s/\s+$//g;  # rtrim
 return $str;
};

# =============================================================================
# Fingerprint  function : trim
# =============================================================================
my $func_field_trim = sub {
 my $str = shift;
 my $chr = shift;
 my $rep = shift;
 $str =~ s/$chr/$rep/g;  # chr by rep
 $str =~ s/^\s+//g;      # ltrim
 $str =~ s/\s+$//g;      # rtrim
 return $str;
};

# =============================================================================
# Fingerprint  function : unpunct
# =============================================================================
my $func_unpunct = sub {
 my $str = shift;
 $str =~ s/-/ /g;                                    # replace hyphens by spaces
 $str =~ s/[[:cntrl:]]|[[:punct:]]|[^[:print:]]//gi; # remove control characters
 return $str;
};

# =============================================================================
# Fingerprint  function : collpase
# =============================================================================
my $func_collapse = sub {
 my $str = shift;
 $str =~ s/\s+/ /g;   # collapse multiple spaces
 return $str;
};

# =============================================================================
# Fingerprint  function : unspace
# =============================================================================
my $func_unspace = sub {
 my $str = shift;
 $str =~ s/\s+//g;    # remove spaces
 return $str;
};

# =============================================================================
# this is from http://rosettacode.org/wiki/Most_frequent_k_chars_distance#Perl
sub mostFreqHashing {
   my $inputstring = shift ;
   my $howmany = shift ;
   my $outputstring ;
   my %letterfrequencies = findFrequencies ( $inputstring ) ;
   my @orderedChars = sort { $letterfrequencies{$b} <=> $letterfrequencies{$a} ||
      index( $inputstring , $a ) <=> index ( $inputstring , $b ) } keys %letterfrequencies ;
   for my $i ( 0..$howmany - 1 ) {
      $outputstring .= ( $orderedChars[ $i ] . $letterfrequencies{$orderedChars[ $i ]} ) ;
   }
   return $outputstring ;
}

sub findFrequencies {
   my $input = shift ;
   my %letterfrequencies ;
   for my $i ( 0..length( $input ) - 1 ) {
      $letterfrequencies{substr( $input , $i , 1 ) }++ ;
   }
   return %letterfrequencies ;
}

sub mostFreqKSimilarity {
   my $first = shift ;
   my $second = shift ;
   my $similarity = 0 ;
   my %frequencies_first = findFrequencies( $first ) ;
   my %frequencies_second = findFrequencies( $second ) ;
   foreach my $letter ( keys %frequencies_first ) {
      if ( exists ( $frequencies_second{$letter} ) ) {
   $similarity += ( $frequencies_second{$letter} + $frequencies_first{$letter} ) ;
      }
   }
   return $similarity ;
}

sub mostFreqKSDF {
   (my $input1 , my $input2 , my $k , my $maxdistance ) = @_ ;
   return $maxdistance - mostFreqKSimilarity( mostFreqHashing( $input1 , $k) ,
   mostFreqHashing( $input2 , $k) ) ;
}

sub mostFreqKSDFHashed {
   (my $input1 , my $input2 , my $maxdistance ) = @_ ;
   return $maxdistance - mostFreqKSimilarity($input1,$input2) ;
}
# =============================================================================

# =============================================================================
# Fingerprint  function : bag_distance
# calculate the bag distance of the normalized strings
# "String Matching with Metric Trees Using an Approximate Distance"
# http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.66.7639&rep=rep1&type=pdf
# =============================================================================
my $func_bag_distance = sub {
  my $str1 = shift;
  my $str2 = shift;

  # split the strings by spaces
  my @x = split //, $str1;
  my @y = split //, $str2;

  # hash to store the string characters
  my %x_hash;
  my %y_hash;

  # characters frequency
  $x_hash{$_}++ foreach (@x);
  $y_hash{$_}++ foreach (@y);

  # dbag(str1, str2) = max(|x-y|,|y-x|)

  # x-y: drops common elements from y
  exists $y_hash{$_} and $y_hash{$_}-- foreach (@x);

  # y-x: drops common elements from x
  exists $x_hash{$_} and $x_hash{$_}-- foreach (@y);

  # residual elements : |x-y|
  my $x_size = 0;
  $x_size += (exists $x_hash{$_} and $x_hash{$_} > 0 ? $x_hash{$_}: 0) foreach (keys %x_hash);

  # residual elements : |y-x|
  my $y_size = 0;
  $y_size += (exists $y_hash{$_} and $y_hash{$_} > 0 ? $y_hash{$_}: 0) foreach (keys %y_hash);

  # maximum number of elements considering
  # the number of residual elements
  my $max_size   = ($x_size, $y_size)[$x_size < $y_size];

  # normalize the result
  my $x_lenght = $#x + 1;
  my $y_lenght = $#y + 1;
  my $max_lenght = ($x_lenght, $y_lenght)[$x_lenght < $y_lenght];

  return $max_size / $max_lenght;
};

# =============================================================================
# Fingerprint  function : intersection
# =============================================================================
my $func_intersection = sub {
  my $str1 = shift;
  my $str2 = shift;

  # return $str1 if $str2 is not defined
  return $str1 if not defined $str2;

  # split the string into whitespace-separated tokens
  my @tokens1 = split(/ /, $str1);

  # split the string into whitespace-separated tokens
  my @tokens2 = split(/ /, $str2);

  # intersection of two lists of strings in Perl
  # @source https://stackoverflow.com/a/14285625
  my @intersection =
      grep { defined }
          @{ { map { lc ,=> $_ } @tokens1 } }
             { map { lc } @tokens2 };

  # join the tokens back together
  return ($func_trim->(join " ", @intersection));
};

# =============================================================================
# Fingerprint  function : mfh_fingerprint
# =============================================================================
my $func_mfh_fingerprint = sub {
  my $str = shift;
  my $K = shift || 2;

  $str    = $func_unspace->(        # remove spaces
            $str);

  return (mostFreqHashing($str,$K));
};

# =============================================================================
# Fingerprint  function : $func_mfh_array_fingerprint
# =============================================================================
my $func_mfh_tokens_fingerprint = sub {
 my $str = shift;
 my $K = shift || 2;

 # split the string into whitespace-separated tokens
 my @tokens = split(/ /, $str);

 # mfh of each token
 my @mfh_tokens = map { $func_mfh_fingerprint->($_,$K) } @tokens;

 # sort the tokens and remove duplicates
 my %seen = ();
 my @unique = grep { ! $seen{ $_ }++ } sort @mfh_tokens;

 # return fingerprint array
 return @unique;
};

# =============================================================================
# Fingerprint  function : normalize
# =============================================================================
my $func_normalize = sub {
  # this is the Open Refine's way to normalize a string
  # we use here a transliteration approach to normalize non-latin characters
  my $str = shift;
  return $func_trim->(           # remove leading and trailing whitespace
         $func_collapse->(       # collapse multiple spaces
         $func_unpunct->(        # remove all punctuation and control characters
         $func_lowercase->(      # change all characters to lowercase
         $func_transliterate->(  # normalize non-latin characters
         $str)))));
};

# =============================================================================
# Fingerprint  function : sort_uniq
# =============================================================================
my $func_sort_uniq = sub {
  my $str = shift;

  # split the string into whitespace-separated tokens
  my @tokens = split(/ /, $str);

  # sort the tokens and remove duplicates
  my %seen = ();
  my @unique = grep { ! $seen{ $_ }++ } sort @tokens;

  # join the tokens back together
  return ($func_trim->(join " ", @unique));
};

# =============================================================================
# Fingerprint  function : normalized_fingerprint
# =============================================================================
my $func_normalized_fingerprint = sub {
  my $str = shift;
  $str    = $func_normalize->($str); # normalize the string

  # get a sorted version of the string' tokens
  return $func_sort_uniq->($str);
};

# =============================================================================
# Fingerprint  function : ngrams_fingerprint
# =============================================================================
my $func_ngrams_fingerprint = sub {
  # this is the Open Refine's way to calculate a n-gram fingerprint
  # we use here a transliteration approach to normalize non-latin characters
  my $str = shift;
  my $ngram_size = shift || 2;
  $str    = $func_unpunct->(        # remove all punctuation and control characters
            $func_lowercase->(      # change all characters to lowercase
            $func_transliterate->(  # normalize non-latin characters
            $func_unspace->(        # remove spaces
            $str))));

  # ngram_split
  my $hash_r = ngram_counts({punctuation => 1}, $str, $ngram_size);

  my $fingerprint= "";
  # join n-grams back together
  foreach my $key (sort keys %$hash_r ) {
   $fingerprint = $fingerprint . $key;
  }

  return ($func_trim->($fingerprint));
};


# =============================================================================
our @EXPORT_OK = qw(function);
{
  our %function = (
    reverse                    => {ref => \&$func_reverse,                     argc => 1  , type => ':' },
    uppercase                  => {ref => \&$func_uppercase,                   argc => 1  , type => ':' },
    normalize                  => {ref => \&$func_normalize,                   argc => 1  , type => ':' },
    sort_uniq                  => {ref => \&$func_sort_uniq,                   argc => 1  , type => ':' },
    lowercase                  => {ref => \&$func_lowercase,                   argc => 1  , type => ':' },
    unaccent                   => {ref => \&$func_unaccent,                    argc => 1  , type => ':' },
    capitalize                 => {ref => \&$func_capitalize,                  argc => 1  , type => ':' },
    transliterate              => {ref => \&$func_transliterate,               argc => 1  , type => ':' },
    concatenate                => {ref => \&$func_concatenate,                 argc => 2  , type => ':' },
    trim                       => {ref => \&$func_trim,                        argc => 1  , type => ':' },
    field_trim                 => {ref => \&$func_field_trim,                  argc => 3  , type => ':' },
    unpunct                    => {ref => \&$func_unpunct,                     argc => 1  , type => ':' },
    collapse                   => {ref => \&$func_collapse,                    argc => 1  , type => ':' },
    unspace                    => {ref => \&$func_unspace,                     argc => 1  , type => ':' },
    mfh_fingerprint            => {ref => \&$func_mfh_fingerprint,             argc => 2  , type => ':' },
    mfh_tokens_fingerprint     => {ref => \&$func_mfh_tokens_fingerprint,      argc => 2  , type => ':' },
    normalized_fingerprint     => {ref => \&$func_normalized_fingerprint,      argc => 1  , type => ':' },
    ngrams_fingerprint         => {ref => \&$func_ngrams_fingerprint,          argc => 2  , type => ':' },
    intersection               => {ref => \&$func_intersection,                argc => 2  , type => ':' },
    bag_distance               => {ref => \&$func_bag_distance,                argc => 2  , type => ':' },
  );
}

1;
