#!/usr/bin/perl -w
# =============================================================================
# Gaia Worker
# =============================================================================
# Copyright (c) 2017, The CorTexT Platform
# Authors: Cristian Martinez
# License: Perl Artistic License 2.0
# =============================================================================
use strict;                               # Pragma to restrict unsafe constructs
use warnings;                             # Pragma to control optional warnings
use Symbol;                               # manipulate Perl symbols and their names
## =============================================================================
use Pod::Usage;                           # Print usage msg from embedded pod doc
use File::Basename;                       # Parse file paths into dir, name and suffix
# =============================================================================
use utf8;                                 # Enables to type utf8 in program code
use open IN  => ':encoding(utf8)';        # Input stream utf8
use open OUT => ':encoding(utf8)';        # Output stream utf8
use open        ':std';                   # STDIN, STDOUT, STDERR utf8
# =============================================================================
use FindBin qw($RealBin);                 # Locate directory of original perl script
use lib "$FindBin::RealBin/lib";          # Use modules at /lib
use AppConfig qw(:argcount);              # Reading conf files and parsing arguments
use AppConfig::Args;                      # Reading command line arguments
use Data::Dumper;                         # Stringified perl data structures
use Digest::MD5;                          # Perl interface to the MD5 Algorithm
use IO::Handle;                           # IO Handling
use DBI;                                  # Database independent interface for Perl
use URI::Escape qw(uri_escape);           # Percent-encode unsafe characters
use LWP::JSON::Tiny;                      # JSON natively with LWP objects
use Digest::SHA1 qw(sha1_hex);            # Perl interface to the SHA-1 algorithm
use Log::Log4perl qw(:easy);              # Log4j implementation for Perl
use Locale::Country;                      # Standard codes for country identification
use Geo::libpostal qw(:all);              # Perl bindings for libpostal
use File::CountLines qw(count_lines);     # Efficiently count the number of line breaks
use Scalar::Util qw(openhandle);          # General-utility scalar subroutines
use Term::ProgressiveBar;                 # Progress meter on a standard terminal
use Fingerprint;                          # String Encoding and decoding of base64 strings
use Try::Tiny;
# =============================================================================
use Text::CSV_XS;                         # Comma-separated values manipulator
use Text::CSV::Hashify;                   # Turn a CSV file into a Perl hash
use Benchmark;

# =============================================================================
# Environment
# =============================================================================
my $config;                               # setup configuration environment
my $log;                                  # Log4j handle
my $ua;                                   # LWP User agent
my $ua_get;                               # LWP User agent get method
my $csv_in;                               # CSV input reader
my $csv_out;                              # CSV output writer
my $uri_pelias;                           # pelias search endpoint
my $uri_pelias_structured;                # pelias structured search endpoint
my $uri_placeholder;                      # placeholder search endpoint
my %lr = ();                              # Lexical resources
my %benchmark = ();                       # Benchmark results
my %stats = ();                           # Stats results
my %target = ();                          # Geocoding targets
my %service_hook = ();                    # service hooks
my @service_columns  = ();                # service columns
my %address_column   = ();                # input address columns
my $output_columns   = ();                # output column names
my $output_handler   = gensym;            # create a new anonymous typeglob
my $progress_handler = gensym;            # progress file handler
my $stats_handler    = gensym;            # stats file handler
my $benchmark_handler = gensym;           # benchmark file handler
# =============================================================================
use Fcntl;
# =============================================================================
# Constants
# =============================================================================
use constant API_NAME         => 'gaia-worker';          # Program name
use constant API_VER          => '2.0-alpha';            # Program version
use constant CONF_FILE        => "$RealBin/config.ini";  # Configuration file
use constant CONF_LOG         => "$RealBin/log.ini";     # Configuration logging
use constant STD_INPUT        => '-';                    # Read from standard input
use constant DEF_DELIMITER    => ';';                    # Default CSV delimiter
use constant DEF_OUTPUT       => \*STDOUT;               # Default output
use constant RESOURCES        => "$RealBin/resources";   # Lexicak resources
use constant WOS_RESOURCES    => RESOURCES . "/wos";     # WoS resources
# =============================================================================
# @see https://github.com/whosonfirst/whosonfirst-placetypes
# Note 1: do not rely on the following numeric constants
# Note 2: whenever you change the layer hierarchy, also update both
#         layer_types and laye_levels
my %layer_hierarchy = (
  # layer 0
  'planet'        =>  0,        # macro level starts
  # layer 1
  'continent'     =>  1,
  'ocean'         =>  1,
  # layer 2
  'empire'        =>  2,
  # layer 3
  'country'       =>  3,
  # layer 4
  'dependency'    =>  4,
  'marinearea'    =>  4,
  'disputed'      =>  4,
  'timezone'      =>  4,
  # layer 5
  'macroregion'   =>  5,
  # layer 6
  'region'        =>  6,        # meso level starts
  # layer 7
  'macrocounty'   =>  7,
  # layer 8
  'county'        =>  8,
  # layer 9
  'localadmin'    =>  9,
  'metro area'    =>  9,
  # layer 10
  'locality'      =>  10,
  # layer 11
  'postalcode'    =>  11,
  'borough'       =>  11,
  # layer 12
  'macrohood'     =>  12,
  # layer 13
  'neighbourhood' =>  13,
  # layer 14
  'microhood'     =>  14,       # micro level starts
  # layer 15
  'campus'        =>  15,
  # layer 16
  'intersection'  =>  16,
  'street'        =>  16,
  # layer 17
  'building'      =>  17,
  # layer 18
  'address'       =>  18,
  # layer 19
  'venue'         =>  19
);
# =============================================================================
my %layer_types = (
  # layer 0
  'planet'        =>  'macro',  # macro level starts
  # layer 1
  'continent'     =>  'macro',
  'ocean'         =>  'macro',
  # layer 2
  'empire'        =>  'macro',
  # layer 3
  'country'       =>  'macro',
  # layer 4
  'dependency'    =>  'macro',
  'marinearea'    =>  'macro',
  'disputed'      =>  'macro',
  'timezone'      =>  'macro',
  # layer 5
  'macroregion'   =>  'macro',
  # layer 6
  'region'        =>  'meso',   # meso level starts
  # layer 7
  'macrocounty'   =>  'meso',
  # layer 8
  'county'        =>  'meso',
  # layer 9
  'localadmin'    =>  'meso',
  'metro area'    =>  'meso',
  # layer 10
  'locality'      =>  'meso',
  # layer 11
  'postalcode'    =>  'meso',
  'borough'       =>  'meso',
  # layer 12
  'macrohood'     =>  'meso',
  # layer 13
  'neighbourhood' =>  'meso',
  # layer 14
  'microhood'     =>  'micro',  # micro level starts
  # layer 15
  'campus'        =>  'micro',
  # layer 16
  'intersection'  =>  'micro',
  'street'        =>  'micro',
  # layer 17
  'building'      =>  'micro',
  # layer 18
  'address'       =>  'micro',
  # layer 19
  'venue'         =>  'micro'
);
# =============================================================================
my %layer_levels = (
  'macro'         => $layer_hierarchy{'planet'},     #  0 ... 19
  'meso'          => $layer_hierarchy{'region'},     #  6 ... 19
  'micro'         => $layer_hierarchy{'microhood'}   # 14 ... 19
);
# =============================================================================
# init
# =============================================================================
sub get_log_file_name() {
  return $config->log_file;
}

# =============================================================================
# setup_progress_file
# =============================================================================
sub setup_progress_file() {
  # open the progress file
  if ($config->_exists('progress_file')) {
   sysopen($progress_handler, $config->progress_file, O_CREAT | O_WRONLY | O_TRUNC) or
           error("Could not open file to write: '%s'", $config->progress_file);
   $progress_handler->autoflush;
  }
}

# =============================================================================
# close_progress_file
# =============================================================================
sub close_progress_file() {
  # close the progress file
  if ($config->_exists('progress_file')) {
   close($progress_handler)  or
     error("Could not close the progress file: '%s'", $config->output);
  }
}

# =============================================================================
# setup_logging
# =============================================================================
sub setup_logging() {
  # logging config
  Log::Log4perl::init(CONF_LOG);
  $log = Log::Log4perl->get_logger($config->log_name);
  if( $config->verbose ) {
    # inc logging level
    $log->level($DEBUG);
  }
}

# =============================================================================
# setup_libpostal
# =============================================================================
sub setup_libpostal() {
  # the first call to parse_address is a lot slower
  my %address = parse_address("2 Boulevard Blaise Pascal, 93160 Noisy-le-Grand");
}

# =============================================================================
# setup_countries_names
# =============================================================================
sub setup_countries_names() {
    Locale::Country::rename_country('kr' => 'South Korea');
    Locale::Country::rename_country('kp' => 'North Korea');
    Locale::Country::rename_country('ir' => 'Iran');
}

# =============================================================================
# setup_countries_alias
# =============================================================================
sub setup_countries_aliases() {
    
    # Adding new aliases to the Locale::Country module  
    # Irland as United Kingdom is already as an alias 
    Locale::Country::add_country_alias('Australia' ,'Australie');
    Locale::Country::add_country_alias('Bangladesh' ,'East Pakistan');
    Locale::Country::add_country_alias('China' ,'Zhonghua');
    Locale::Country::add_country_alias('China' ,'Kina');
    Locale::Country::add_country_alias('China' ,'Chine');
    Locale::Country::add_country_alias('Bosnia and Herzegovina' ,'Bosnie-Herzégovine');
    Locale::Country::add_country_alias('Finland' ,'Suomi');
    Locale::Country::add_country_alias('France' ,'Francia');
    Locale::Country::add_country_alias('Germany' ,'Alemania');
    Locale::Country::add_country_alias('Germany' ,'Allemagne');
    Locale::Country::add_country_alias('Germany' ,'Deutschland');
    Locale::Country::add_country_alias('Germany' ,'Bundesrepublik Deutschland');
    Locale::Country::add_country_alias('India' ,'Hindustan');
    Locale::Country::add_country_alias('Italy' ,'Italia');
    Locale::Country::add_country_alias('Italy' ,'Italie');
    Locale::Country::add_country_alias('Japan' ,'Nippon');
    Locale::Country::add_country_alias('Japan' ,'Japon');
    Locale::Country::add_country_alias('Spain' ,'Espagne');
    Locale::Country::add_country_alias('South Korea' ,'Corée du Sud'); 
    Locale::Country::add_country_alias('South Korea' ,'Corea del Sur');
    Locale::Country::add_country_alias('Netherlands' ,'Pays-Bas');
    Locale::Country::add_country_alias('Netherlands' ,'Paises Bajos');
    Locale::Country::add_country_alias('New Zealand' ,'Nouvelle-Zélande');
    Locale::Country::add_country_alias('Russian Federation' ,'Russie');
    Locale::Country::add_country_alias('Russian Federation' ,'Rusia');
    Locale::Country::add_country_alias('Switzerland', 'Suisse');
    Locale::Country::add_country_alias('Switzerland' ,'Suiza');
    Locale::Country::add_country_alias('United Kingdom' ,'Reino Unido');
    Locale::Country::add_country_alias('United Kingdom' ,'Royaume-Uni');
    Locale::Country::add_country_alias('United Kingdom' ,'England');
    Locale::Country::add_country_alias('United Kingdom' ,'Scotland');
    Locale::Country::add_country_alias('United Kingdom' ,'Wales'); 
    Locale::Country::add_country_alias('United Kingdom' ,'United Kingdom of Great Britain and Northern Ireland (the)'); 
    Locale::Country::add_country_alias('United Kingdom' ,'United Kingdom of Great Britain and Northern Ireland'); 
    Locale::Country::add_country_alias('United Kingdom' ,'The United Kingdom of Great Britain and Northern Ireland'); 
    Locale::Country::add_country_alias('United States' ,'États-Unis'); 
    Locale::Country::add_country_alias('United States' ,'Estados Unidos');
    Locale::Country::add_country_alias('Vietnam' ,'Nam viet');
}

# setup_benchmark
# ============================================================================
sub setup_useragent() {
  # instance the user agent
  $ua = LWP::UserAgent::JSON->new;

  # set the default header
  $ua->default_header(
      'Accept-Language' => 'en-US',
      'Accept-Charset'  => 'utf-8',
      'Accept-Encoding' => 'gzip, deflate',
  );
}

# =============================================================================
# ua_get_benchmark
# =============================================================================
sub ua_get_benchmark() {
  my $url = shift;
  # start time
  $benchmark{'net'}{'t_start'} = Benchmark->new;
  # dispatch a GET request on the given URL
  my $response = $ua->get( $url );
  # end time
  $benchmark{'net'}{'t_end'}  = Benchmark->new;
  # diff time
  $benchmark{'net'}{'t_diff'} = timediff($benchmark{'net'}{'t_end'},
                                         $benchmark{'net'}{'t_start'});
  # total time = total time + diff time
  $benchmark{'net'}{'total'} =     ref $benchmark{'net'}{'total'} ?
                               timesum($benchmark{'net'}{'total'},
                                       $benchmark{'net'}{'t_diff'}) :
                                       $benchmark{'net'}{'t_diff'};
  # increse the number of network requests
  $benchmark{'net'}{'count'}++;
  return $response;
}

# =============================================================================
# setup_targets
# =============================================================================
sub setup_targets() {
  # initilize targets
  if ($config->_exists('target_countries')  &&
      $config->target_countries ne 'all') {
    # get target countries
    my @target_countries = split(',', $config->target_countries);
    #
    foreach my $iso3 (@target_countries) {
      $iso3 = lc $Fingerprint::function{'trim'}{'ref'}($iso3);
      if (exists $lr{'common'}{'country'}{'iso3'}{$iso3}) {
        $target{'country'}{uc $iso3} = 1;
      } else {
        error("Unknown alpha-3 country code: '%s' for target_countries option", $iso3);
      }
    }
  } else {
    # explicitly set target_countries to all
    $config->set('target_countries', 'all');
  }
}

# =============================================================================
# setup_stats
# =============================================================================
sub setup_stats() {
  # counters initialisation
  $stats{'ungeocoded'} = 0;
  $stats{'confidence'} = 0;

  # only when print stats is requested
  if ($config->_exists('stats_print') &&
      $config->stats_print eq 'on') {
    # create the stats file
    sysopen($stats_handler, $config->stats_file, O_CREAT | O_WRONLY | O_TRUNC) or
            error("Could not open file to write: '%s'", $config->stats_file);
  } else {
    # explicitly disable benchmark_all
    $config->set('stats_print', 'off');
  }
}

# =============================================================================
# ua_get_no_benchmark
# =============================================================================
sub ua_get_no_benchmark() {
  # dispatch a GET request on the given URL
  return $ua->get(shift);
}

# =============================================================================
# setup_benchmark
# =============================================================================
sub setup_benchmark() {
  # enable benchmark_global when benchmark_all
  if ($config->_exists('benchmark_all') &&
      $config->benchmark_all eq 'on') {
      # counters initialisation
      $benchmark{'net'}{'count'} = 0;
      # ua_get function with benchmarking
      $ua_get = \&ua_get_benchmark;
      # enable benchmark_global when benchmark_all
      $config->set('benchmark_global', 'on');
  } else {
      # explicitly disable benchmark_all
      $config->set('benchmark_all', 'off');
      # ua_get function without benchmarking
      $ua_get = \&ua_get_no_benchmark;
  }

  # explicitly disable benchmark_global
  if (not $config->_exists('benchmark_global') ||
      $config->benchmark_global ne 'on') {
      $config->set('benchmark_global', 'off');
  }

  # create the benchmark file
  if ($config->benchmark_global eq 'on') {
    sysopen($benchmark_handler, $config->benchmark_file, O_CREAT | O_WRONLY | O_TRUNC) or
           error("Could not open file to write: '%s'", $config->benchmark_file);
  }
}

sub print_params() {

        my $geo_method = $config->geocoding_mode;
        
        if ($geo_method eq 'norm-code') {
                $geo_method = 'Filtering non geographical information';
        } elsif ($geo_method eq 'vanilla') {
                $geo_method = 'No customization';
        } elsif ($geo_method eq 'macro-meso') {
                $geo_method = 'Priority to the city';
        } elsif ($geo_method eq 'macro-micro') {
                $geo_method = 'Priority to the street level';
        }

        my $default_columns = 'label;longitude;latitude;confidence;accuracy;layer;city;region;country;iso3';
        
        my $address = $config->csv_in_address;
        my $top_scale = $config->threshold_layer;
        my $threshold_confidence = $config->threshold_confidence;
        my $max_num_addr = $config->count;
        my $output_columns = $config->columns;
        my $advanced_setting = 'Yes';

        if(($output_columns eq $default_columns) &&
           $max_num_addr eq '-1' &&
           $threshold_confidence eq '0.5') {
            $advanced_setting = 'Default';             
        }

        my $message = <<"END_MESSAGE";

                           Parameters:
                                            - Name of the field which contains addresses: $address
                                            - Top scale filter: $top_scale
                                            - Geocoding methods: $geo_method
                                            - Advanced settings: Default
                                                - Output columns: $output_columns
                                                - Max number of addresses to process: $max_num_addr
                                                - Confidence threshold: $threshold_confidence
END_MESSAGE
 
        info($message);
}

sub print_summary() {
        
        # number of addresses processed
        my $gcount = $benchmark{'global'}{'count'};
        # number of addresses geocoded
        my $gaddresses = $gcount - $stats{'ungeocoded'};
        my $geocoded = ($gaddresses/$gcount) * 100;
        my $message = <<"END_MESSAGE";

                           Summary:
                                            - Total addresses: $gcount
                                            - Geocoded: $geocoded% 
END_MESSAGE

        info($message);
}

# =============================================================================
# print_stats
# =============================================================================
sub print_stats() {
  # return when the global stats_print option is disabled
  return if $config->stats_print ne 'on';

  # number of addresses processed
  my $gcount = $benchmark{'global'}{'count'};

  # print the total of addresses at the input
  printf $stats_handler "Total addresses: %d\n",       $gcount;

  # print input config
  printf $stats_handler "Geocoding source: %s\n",      $config->geocoding_source;
  printf $stats_handler "Geocoding mode: %s\n",        $config->geocoding_mode;
  printf $stats_handler "Threshold confidence: %s\n",  $config->threshold_confidence;
  printf $stats_handler "Threshold layer: %s\n",       $config->threshold_layer;
  printf $stats_handler "Target countries: %s\n",   uc $config->target_countries;

  # number of addresses geocoded
  my $gaddresses = $gcount - $stats{'ungeocoded'};

  # print country stats
  foreach my $iso3 ( sort { $stats{'countries'}{$b}{'count'} <=>
                            $stats{'countries'}{$a}{'count'} }
                            keys %{$stats{'countries'}}) {
    printf $stats_handler "Country %s: %.2f%%\n", $iso3,
           ($stats{'countries'}{$iso3}{'count'}/$gaddresses) * 100;
  }

  # print layers stats
  foreach my $type ( sort { $stats{'layers'}{$b} <=>
                            $stats{'layers'}{$a} }
                            keys %{$stats{'layers'}}) {
    printf $stats_handler "Layer %s: %.2f%%\n", ucfirst $type,
           ($stats{'layers'}{$type}/$gaddresses) * 100;
  }

  # geocoded confidence
  my $gconfidence = $stats{'confidence'} / ($gaddresses + 0.001);

  # print geocoded stats
  printf $stats_handler "Geocoded: %.2f%%\n", ($gaddresses/$gcount) * 100;

  # number of addresses on target
  my $taddresses = $config->target_countries eq 'all' ?
                   $gaddresses :
                   0;

  # target confidence
  my $tconfidence = 0;

  # compute target stats
  if ($config->target_countries ne 'all') {
    # loop over each retrived country
    foreach my $iso3 (keys %{$stats{'countries'}}) {
      if (exists $target{'country'}{$iso3}) {
        $taddresses  += $stats{'countries'}{$iso3}{'count'};
        $tconfidence += $stats{'countries'}{$iso3}{'confidence'};
      }
    }
  }

  # compute target confidence against the number of addresses geocoded
  $tconfidence = $config->target_countries eq 'all' ?
                 $gconfidence :
                 $tconfidence / ($gaddresses + 0.001);

  # print target stats
  if ($config->target_countries ne 'all') {
    printf $stats_handler "Precision: %.2f\n",  $taddresses/$gcount;
  }
  printf $stats_handler "Confidence: %.2f\n", $tconfidence;

  # close the stats file
  close($stats_handler)  or
  error("Could not close the stats file: '%s'", $config->stats_file);
}

# =============================================================================
# print_benchmark
# =============================================================================
sub print_benchmark() {
  # return when the global benchmark option is disabled
  return if $config->benchmark_global ne 'on';

  # global end time
  $benchmark{'global'}{'t_end'}  = Benchmark->new;

  # global diff time
  $benchmark{'global'}{'t_diff'} = timediff($benchmark{'global'}{'t_end'},
                                            $benchmark{'global'}{'t_start'});

  # global (net+calc) benchmark
  my($gr, $gpu, $gps, $gcu, $gcs, $gn) = @{$benchmark{'global'}{'t_diff'}};
  my $gcpu   = $benchmark{'global'}{'t_diff'}->cpu_a;
  my $gcount = $benchmark{'global'}{'count'};

  # print the header
  printf $benchmark_handler "Type;Wallclock;App;Sys;CPU;Count\n";

  # compute calc and net results
  if ($config->benchmark_all eq 'on' && defined $benchmark{'net'}{'total'}) {
    # net requests benchmark
    my($nr, $npu, $nps, $ncu, $ncs, $nn) = @{$benchmark{'net'}{'total'}};
    my $ncpu   = $benchmark{'net'}{'total'}->cpu_a;
    my $ncount = $benchmark{'net'}{'count'};

    # calc (global-net) processing benchmark
    $benchmark{'calc'}{'t_diff'} = timediff($benchmark{'global'}{'t_diff'},
                                            $benchmark{'net'}{'total'});
    my($cr, $cpu, $cps, $ccu, $ccs, $cn) = @{$benchmark{'calc'}{'t_diff'}};
    my $ccpu = $benchmark{'calc'}{'t_diff'}->cpu_a;

    # print Calc and Net
    printf $benchmark_handler "%s;%.2f;%.2f;%.2f;%.2f;%d\n",'Calc',$gr,$cpu,$cps,$ccpu,$gcount;
    printf $benchmark_handler "%s;%.2f;%.2f;%.2f;%.2f;%d\n",'Net', $nr,$npu,$nps,$ncpu,$ncount;
  }

  # print Global
  printf $benchmark_handler "%s;%.2f;%.2f;%.2f;%.2f;%d\n",'Global',$gr,$gpu,$gps,$gcpu,$gcount;

  # close the benchmark file
  close($benchmark_handler)  or
  error("Could not close the benchmark file: '%s'", $config->benchmark_file);
}

# =============================================================================
# setup_services
# =============================================================================
sub setup_services() {
  # prepare the document-service request (text)
  $uri_pelias = URI->new(sprintf('http%s://%s:%s/%s/%s',
            $config->pelias_https ne "off" ? "s" : "",
            $config->pelias_host,
            $config->pelias_port,
            $config->pelias_version,
            "search"
  ));

  # prepare the document-service request  (structured)
  $uri_pelias_structured = URI->new(sprintf('http%s://%s:%s/%s/%s/%s',
            $config->pelias_https ne "off" ? "s" : "",
            $config->pelias_host,
            $config->pelias_port,
            $config->pelias_version,
            "search",
            "structured"
  ));

  # prepare the placeholder-service request  (parser/search)
  $uri_placeholder = URI->new(sprintf('http%s://%s:%s/%s/%s',
            $config->placeholder_https ne "off" ? "s" : "",
            $config->placeholder_host,
            $config->placeholder_port,
            "parser",
            "search"
  ));
}

# =============================================================================
# load_dictionary
# =============================================================================
sub load_dictionary {
  my $source = shift;
  my $class  = shift;
  my $fname  = shift;
  my $key    = shift || 'abbreviation';
  my $value  = shift || 'meaning';
  my $name   = shift || $fname;
  my $file   = sprintf("%s/%s/%s/%s.csv", RESOURCES, $source, $class, $fname);

  # if the file exists and it is in readable form
  if (-e $file and -T $file) {
   # dictionnary
   $lr{$source}{$class}{$name}{'dict'} = hashify($file, $key) or
        error("Cannot use CSV: %s", Text::CSV->error_diag ());

   # regex pattern
   my $regex_pattern = join '|', map quotemeta,
                                 keys %{ $lr{$source}{$class}{$name}{'dict'} };

   # precompile regex
   $lr{$source}{$class}{$name}{'regex'} = qr/$regex_pattern/;

   # xeger pattern
   my $xeger_pattern = join '|', map {
                       quotemeta
                       $Fingerprint::function{'normalize'}{'ref'}(
                       $lr{$source}{$class}{$name}{'dict'}{$_}{$value})}
                       keys %{ $lr{$source}{$class}{$name}{'dict'} };

   # precompile regex
   $lr{$source}{$class}{$name}{'xeger'} = qr/$xeger_pattern/;

   # loaded
   return 1;
  }

  # nothing loaded
  return 0;
}

# =============================================================================
# setup_resources
# =============================================================================
sub setup_resources() {
  # languages
  $lr{'common'}{'lang'} = hashify(RESOURCES . '/langs.csv', 'lang') or
                          error("Cannot use CSV: %s", Text::CSV->error_diag ());

  # countries iso3
  $lr{'common'}{'country'}{'iso3'} = hashify(RESOURCES . '/countries.csv', 'iso3') or
                                     error("Cannot use CSV: %s", Text::CSV->error_diag ());

  # countries names
  $lr{'common'}{'country'}{'name'} = hashify(RESOURCES . '/countries.csv', 'name') or
                                     error("Cannot use CSV: %s", Text::CSV->error_diag ());

  # countries names regex pattern
  my $regex_pattern = join '|', map quotemeta, keys %{ $lr{'common'}{'country'}{'name'} };

  # countries names precompile regex
  $lr{'common'}{'country'}{'name_regex'} = qr/$regex_pattern/i;

  # Common resouces to all kind of sources
  if ($config->geocoding_mode ne 'vanilla') {
    # addresses
    load_dictionary('all', 'addresses', 'common');

    # organisations
    load_dictionary('all', 'addresses', 'common');

    # countries
    foreach my $key (keys %{$lr{'common'}{'country'}{'iso3'}}) {
      load_dictionary('all', 'countries', $key);
    }

    # languages
    foreach my $key (keys %{$lr{'common'}{'lang'}}) {
      load_dictionary('all', 'langs', $key);
    }
  }

  # WoS Resources
  if ($config->geocoding_source eq 'wos') {
    # organisations
    load_dictionary('wos', 'addresses', 'common');

    # countries
    foreach my $key (keys %{$lr{'common'}{'country'}{'iso3'}}) {
      load_dictionary('wos', 'countries', $key);
    }

    # languages
    foreach my $key (keys %{$lr{'common'}{'lang'}}) {
      load_dictionary('wos', 'langs', $key);
    }
  }  # if ($config->geocoding_source eq 'wos')
}

# =============================================================================
# setup_csv_reader
# =============================================================================
sub setup_csv_handles() {
 # CSV input reader
 $csv_in   = Text::CSV_XS->new ({
                sep_char    => $config->csv_in_sep_char,
                eol         => "\n",
                decode_utf8 => 1,
                escape_char => "\\",
                quote_char  => '"',
                allow_loose_quotes  => 1,
                allow_loose_escapes => 1,
                binary      => 1,})
                or error("Cannot use CSV: %s", Text::CSV->error_diag ());

 # CSV output writer
 $csv_out   = Text::CSV_XS->new ({
                sep_char    => $config->csv_out_sep_char,
                eol         => "\n",
                decode_utf8 => 1,
                escape_char => "\\",
                quote_char  => '"',
                binary      => 1,})
                or error("Cannot use CSV: %s", Text::CSV->error_diag ());

 # CSV output file
 # check if an output argument was passed
 if (openhandle($config->output)) {
  $output_handler = $config->output;
  # force to disable the progress bar if STDOUT is probably not redirected
  if ($config->output == \*STDOUT and -t STDOUT) {
    $config->set('progress_bar', 'off');
  }
  # force to have --output for --database
  if ($config->database ne "") {
   error("argument --database DB requires --output FILE");
  }
 } else {
  sysopen($output_handler, $config->output, O_CREAT | O_WRONLY | O_TRUNC) or
          error("Could not create csv file to write: '%s'", $config->output);
 }
}

# =============================================================================
# setup_columns
# =============================================================================
sub setup_columns() {
 # check if the header argument exists
 if (not $config->_exists('columns')) {
   error("header option is required");
 }

 # get column names
 @service_columns = split($config->csv_out_sep_char, $config->columns);

 # setup service output hooks
 foreach my $column_name (@service_columns) {
  my $hook_name = 'hook_' . $column_name;

  # test if a hook for the current column exists
  if (not $config->_exists($hook_name)) {
    error("there is not a hook for the column '%s'", $column_name);
  }

  # hook attributes
  my $hook      = $config->get($hook_name);
  my $is_sub    = ($hook =~ /^sub/ || 0);
  my $type_ref  = $config->database_driver . '_' . $column_name;

  # test if a db type exists for the current column
  if (not $config->_exists($type_ref)) {
    error("there is not a %s type for the column '%s'", $config->database_driver, $column_name);
  }

  # create the hook
  $service_hook{$column_name} = {'hook'   => $hook,
                                 'is_sub' => $is_sub,
                                 'type'   => $config->get($type_ref)};

  # push the current column
  push @{$output_columns}, $column_name;
 }
}

# =============================================================================
# init
# =============================================================================
sub init() {
 setup_logging();            # setup logging
 info("Starting...");        # starting
 setup_progress_file();      # setup progress file
 setup_csv_handles();        # setup CSV reader/writer
 setup_columns();            # setup output header
 setup_services();           # setup services URIs
 setup_resources();          # setup worker resources
 setup_libpostal();          # first call to libpostal
 setup_countries_names();    # setup names when it is not accurate
 setup_countries_aliases();  # setup aliases to detect countries
 setup_useragent();          # setup user agent
 setup_targets();            # setup geocoding targets
 setup_stats();              # setup stats
 setup_benchmark();          # setup benchmark
}

# =============================================================================
# finish
# =============================================================================
sub finish() {
 info("Successful!");
 close_progress_file();      # close progress file
}

# =============================================================================
# debug
# =============================================================================
sub debug {
  my $message = shift;
  # print a debug message
  $log->debug(sprintf($message, @_));
}

# =============================================================================
# info
# =============================================================================
sub info {
  my $message = shift;
  # print a debug message
  $log->info(sprintf($message, @_));
}

# =============================================================================
# warning
# =============================================================================
sub warning {
  my $message = shift;
  # print a warning message
  $log->warn(sprintf($message, @_));
}

# =============================================================================
# error
# =============================================================================
sub error {
  my $message = shift;
  # print an error message
  if(defined $log) {
   $log->error(sprintf($message, @_));
  } else {
   printf STDERR $message . "\n", @_;
  }

  # exit with error
  exit(1);
}

# =============================================================================
# check_file
# =============================================================================
sub check_file{
 my $varname  = shift;
 my $file     = shift;

 # get filename without path
 my $basename = basename($file);

 # test is stdin
 return 1                                     if($file eq STD_INPUT);
 # test directory
 error("'%s' is a directory.", $file)                  if (-d $file);
 # test exist
 error("File '%s' does not exist.", $basename)      unless(-e $file);
 # test readable
 error("Cannot read '%s' file.", $basename)         unless(-r $file);
 # nonzero size
 error("File '%s' is empty.", $basename)            unless(-s $file);
 # test plain-format
 error("File '%s' format is not valid.", $basename) unless(-T $file);

 # return true
 return 1;
}

# =============================================================================
# show_config
# =============================================================================
sub show_config{
  my $value;                                 # current value of a variable
  $Data::Dumper::Terse  = 1;                 # don't output string '$VAR'
  $Data::Dumper::Indent = 0;                 # spews output without any newlines

  my %varlist = $config->varlist('.*');      # get all confuration options
  foreach my $varname (sort keys %varlist){  # sort and print names and values
    $value = Dumper $config->get($varname);  # capture readable var structure
    printf "%-27s%s\n", $varname, $value;    # print padded info to stdout
  }

  exit 2;                                    # exit with error 2
}

# =============================================================================
# show_version
# =============================================================================
sub show_version(){
  my $program = basename($0);              # set program name to script name
  printf "$program version %s\n",API_VER;
  exit 0;
}

# =============================================================================
# process_argfiles()
# =============================================================================
sub process_argfiles {
  # if there are more  arguments to be processed
  if(scalar(@ARGV) > 0){
    # treat last arguments as filenames
    # each step remove a filename from @ARGV
    while (my $arg = shift @ARGV) {
        # try to expand the filename, e.g."*.txt"
        my @filelist = glob($arg);

        # if glob expansion does not match any file
        if(scalar(@filelist) == 0){
          # intentionally raising an error to exit
          $config->filelist($arg);
        }

        # add each file to filelist using config environment
        # filelist() automatically call check_file() function
        foreach my $file (@filelist) {
          $config->filelist($file);
        }
    }
    # finally put all expanded filenames back in argv
    @ARGV = map {$_} @{$config->filelist()};
  }

  if(-t STDIN and not @ARGV){
    error("No files were selected, try --help to show options");
  }
}

# =============================================================================
# setup_configuration()
# =============================================================================
sub setup_configuration() {
  $config = AppConfig->new( {       #
    CASE      => 1,                 #
    CREATE    => 1,                 #
    PEDANTIC  => 1,                 #
    ERROR     => \&error,           #
    GLOBAL => {                     #
        DEFAULT  => "<undef>",      #
        ARGCOUNT => ARGCOUNT_ONE,   #
    }
  });

  # define
  $config->define("filelist|f|file=s@"              ,{ VALIDATE => \&check_file   });
  $config->define("csv_in_parsed!"                  ,{ DEFAULT  => 0              });
  $config->define("help|?!"                         ,{ DEFAULT  => 0              });
  $config->define("man|m!"                          ,{ DEFAULT  => 0              });
  $config->define("output|o=s"                      ,{ DEFAULT  => DEF_OUTPUT     });
  $config->define("database|d=s"                    ,{ DEFAULT  => ''             });
  $config->define("database_user"                   ,{ DEFAULT  => ''             });
  $config->define("database_password"               ,{ DEFAULT  => ''             });
  $config->define("show-config|S|show!"             ,{ DEFAULT  => 0              });
  $config->define("verbose|v!"                      ,{ DEFAULT  => 0              });
  $config->define("version|V!"                      ,{ DEFAULT  => 0              });
}

# =============================================================================
# check_options()
# =============================================================================
sub check_options {
  # check geocoding mode
  if ((not $config->_exists('geocoding_mode'))  ||
     ($config->geocoding_mode ne 'vanilla'      &&
      $config->geocoding_mode ne 'macro-meso'   &&
      $config->geocoding_mode ne 'norm-code'   &&
      $config->geocoding_mode ne 'macro-micro')) {
   error("Invalid geocoding mode: '%s'", $config->_exists('geocoding_mode') ?
                                         $config->geocoding_mode : 'undef');
 }
}

# =============================================================================
# process_options()
# =============================================================================
sub process_options {
  $config->file(CONF_FILE);                                # load conf from file

  my $arg_count = scalar(@ARGV);                           # count args
  $config->getopt(qw(no_ignore_case)) ;                    # load conf from @ARGV

  show_version()           if( $config->version);          # show program version
  show_config()            if( $config->show);             # show configuration
  pod2usage(-verbose => 2) if( $config->man);              # show manual
  pod2usage(-verbose => 0) if(($config->help) ||           # show help
                           (-t STDIN and not $arg_count)); # reading from stdin?

  process_argfiles();      # save the remaining arguments as filenames
  check_options();         # extra cheking options
}

# =============================================================================
# create_database()
# ============================================================================
sub create_database($) {
  my $total_of_records = shift;

  # continue only if there are records to process
  return 1 if $total_of_records <= 0;

  # simple progress bar on the terminal
  my $progress = Term::ProgressiveBar->new({name      => 'exporting',
                                            count     => $total_of_records,
                                            silent    => $config->progress_bar eq 'off',
                                            remove    => 1,
                                            fh        => \*STDERR,
                                            ETA       => 'linear',
                                            on_update => \&on_progress_update,});

  # This value is taken as being the maximum speed between updates
  # to aim for. It is only meaningful if ETA is switched on
  $progress->max_update_rate(1);

  # switches off the use of minor characters
  $progress->minor(0);

  # to only call $progress->update again when needed
  my $next_update = 0;

  my $dsn = sprintf("DBI:%s:dbname=%s", $config->database_driver, $config->database);

  my $dbh = DBI->connect($dsn,
                         $config->database_user,
                         $config->database_password,
                         { RaiseError => 1 })
                         or error($DBI::errstr);

  my $stmt;
  my $rv;

  # drop table if already exists
  $stmt = "DROP TABLE IF EXISTS " . $config->database_table;
  $rv   = $dbh->do($stmt) or error($DBI::errstr);

  # create a new table
  $stmt = "CREATE TABLE " . $config->database_table . "(";
  foreach my $column_name (@{$output_columns}) {
    $stmt .= sprintf("%s %s,", $column_name,
              scalar $service_hook{$column_name}{'type'} ?
                     $service_hook{$column_name}{'type'} :
                     $config->_exists($config->database_driver . '_' . $column_name) ?
                     $config->get($config->database_driver . '_' . $column_name) :
                     $config->get($config->database_driver . '__default'));
  }
  $stmt  =~ s/,$//;
  $stmt .= ");";
  $rv   = $dbh->do($stmt) or error($DBI::errstr);

  # try to open the input (old output) file
  sysopen(INPUT, $config->output, O_RDONLY) or
          error("Could not open csv file: '%s'", $config->output);

  # get the csv header
  my @columns = $csv_in->header (*INPUT);

  # prepate the insert statment
  $stmt = "INSERT INTO " . $config->database_table ." ("
        . join( ',', @columns )           . ") VALUES ("
        . join( ',', ( '?' ) x @columns ) . ")";

  my $sth = $dbh->prepare($stmt) or error($DBI::errstr);

  # to keep track the number of addresses processed
  my $count = 0;

  # loop line by line
  while ( my $row = $csv_in->getline( *INPUT ) ) {
    # execute the insert statment
    $sth->execute(@{$row}) or warning($DBI::errstr);

    # increase the number of inserted rows
    $count++;

    # only call update() again when needed
    if ($count >= $next_update) {
      $next_update = $progress->update($count);
    }
  }

  # try to close the input file
  close(INPUT)  or error("Could not close input file: '%s'", $config->output);

  # close the database
  $dbh->disconnect();

  # ensure that the bar ends on 100%
  $progress->update($total_of_records) if $total_of_records > $next_update;
}

# =============================================================================
# csv_count()
# ============================================================================
sub csv_count() {
  # check if count was already passed as an argument
  if ($config->_exists('count') and
      $config->count != -1) {
    return $config->count;
  }

  # try to count the number of records
  my $total_of_records = 0;

  # loop through each csv file
  foreach my $file (@ARGV) {
   $total_of_records += count_lines($file);
  }
  # each csv file must have a header, thus the number
  # of headings is equal to the number of files,
  # headings are not counted as records, we decrease
  # here the number of records according to the number
  # of headings
  $total_of_records -= scalar @ARGV;

  return $total_of_records;
}

# =============================================================================
# libpostal parse_address with structured geocoding labels
# =============================================================================
sub parse_address_convert($) {
  my $address = shift;
  my %parsed  = parse_address($address);

  # workaround to patch lipostal issues while parsing WoS' addresses
  if ($config->geocoding_source eq 'wos') {
   # country is always present
   if (not exists $parsed{'country'} and
           exists $parsed{'locality'}) {
     $parsed{'country'} =  delete $parsed{'locality'};
   }

   # country is always present
   if (not exists $parsed{'country'} and
           exists $parsed{'road'}) {
     $parsed{'country'} =  delete $parsed{'road'};
   }

   # we are looking only for postal codes
   if (exists $parsed{'house_number'}) {
     $parsed{'postcode'} =  delete $parsed{'house_number'};
   }

   # we are looking only for cities
   if (exists $parsed{'road'}) {
     $parsed{'city'} =  $parsed{'road'} . (exists $parsed{'city'}  ?
                                    " " . $parsed{'city'} : "");
     delete $parsed{'road'};
   }

   # we are looking only for cities
   if (exists $parsed{'house'}) {
     $parsed{'city'} =  $parsed{'house'} . (exists $parsed{'city'}  ?
                                     " " . $parsed{'city'} : "");
     delete $parsed{'house'};
   }
  }  # ($config->geocoding_source eq 'wos')

  # add a new key to store information about the existence of layers
  $parsed{'layers'}  = {'micro'   => 0,
                        'macro'   => 0,
                        'postal'  => 0,
                        'country' => 0};

  # comments below were taken from
  # @see https://github.com/openvenues/libpostal

  # test if we have parsed a micro-layer
  # house: venue name e.g. "Brooklyn Academy of Music"
  # category: for category queries like "restaurants", etc.
  # near: phrases like "in", "near", etc.
  # house_number: usually refers to the external (street-facing) building number.
  # road: street name(s)
  # unit: an apartment, unit, office, lot, or other secondary unit designator
  # level: expressions indicating a floor number e.g. "3rd Floor", etc.
  # staircase: numbered/lettered staircase
  # entrance: numbered/lettered entrance
  $parsed{'layers'}{'micro'} = exists $parsed{'house'}        ||
                               exists $parsed{'category'}     ||
                               exists $parsed{'house_number'} ||
                               exists $parsed{'road'}         ||
                               exists $parsed{'unit'}         ||
                               exists $parsed{'level'}        ||
                               exists $parsed{'staircase'}    ||
                               exists $parsed{'entrance'}     ||
                               $parsed{'layers'}{'micro'};

  # test if we have parsed a postal reference-layer
  # po_box: post office box: typically found in non-physical (mail-only) addresses
  # postcode: postal codes used for mail sorting
  $parsed{'layers'}{'postal'} = exists $parsed{'po_box'}       ||
                                exists $parsed{'postcode'}     ||
                                $parsed{'layers'}{'postal'};

  # test if we have parsed a macro-layer
  # suburb: usually an unofficial neighborhood name like "Harlem" or "Crown Heights"
  # city_district: these are usually boroughs or districts within a city
  # city: any human settlement including cities, towns, villages, hamlets, localities, etc.
  # island: named islands e.g. "Maui"
  # state_district: usually a second-level administrative division or county.
  # state: a first-level administrative division.
  # country_region: informal subdivision of a country without any political status
  $parsed{'layers'}{'macro'} = exists $parsed{'suburb'}         ||
                               exists $parsed{'city_district'}  ||
                               exists $parsed{'city'}           ||
                               exists $parsed{'island'}         ||
                               exists $parsed{'state_district'} ||
                               exists $parsed{'state'}          ||
                               exists $parsed{'country_region'} ||
                               $parsed{'layers'}{'macro'};

  # country: sovereign nations and their dependent territories, anything with an ISO-3166 code.
  $parsed{'layers'}{'country'} = exists $parsed{'country'} ? 1 : 0;

  # @see https://mapzen.com/documentation/search/structured-geocoding
  # structured geocoding to parser labels
  # address       => house_number + house + road + unit + level + staircase + entrance
  # neighbourhood => suburb
  # borough       => city_district
  # locality      => city
  # county        => island, state_district
  # region        => world_region, country_region, state
  # postalcode    => postcode
  # country       => country
  $parsed{'neighbourhood'} =  delete $parsed{'suburb'}         if exists $parsed{'suburb'};
  $parsed{'borough'}       =  delete $parsed{'city_district'}  if exists $parsed{'city_district'};
  $parsed{'locality'}      =  delete $parsed{'city'}           if exists $parsed{'city'};
  $parsed{'county'}        =  delete $parsed{'island'}         if exists $parsed{'island'};
  $parsed{'county'}        =  delete $parsed{'state_district'} if exists $parsed{'state_district'};
  $parsed{'region'}        =  delete $parsed{'state'}          if exists $parsed{'state'};
  $parsed{'region'}        =  delete $parsed{'country_region'} if exists $parsed{'country_region'};
  $parsed{'postalcode'}    =  delete $parsed{'postcode'}       if exists $parsed{'postcode'};

  # world_region: currently only used for appending “West Indies” after the country name
  delete $parsed{'world_region'} if exists $parsed{'world_region'};

  return %parsed;
}

# =============================================================================
# placeholder lineage with structured geocoding labels
# =============================================================================
sub parse_lineage_convert($) {
   my $lineage = shift;
   my %parsed  = ();

  # neighbourhood
  $parsed{'neighbourhood'} = $lineage->{'neighbourhood'}{'name'} if exists
                             $lineage->{'neighbourhood'}{'name'};

  # postalcode
  $parsed{'postalcode'} = $lineage->{'postalcode'}{'name'} if exists
                          $lineage->{'postalcode'}{'name'};

  # borough
  $parsed{'borough'}    = $lineage->{'borough'}{'name'} if exists
                          $lineage->{'borough'}{'name'};

  # locality
  $parsed{'locality'}   = $lineage->{'locality'}{'name'} if exists
                          $lineage->{'locality'}{'name'};

  # county
  $parsed{'county'}     = $lineage->{'county'}{'name'} if exists
                          $lineage->{'county'}{'name'};

  # region
  $parsed{'region'}     = $lineage->{'region'}{'name'} if exists
                          $lineage->{'region'}{'name'};

  # country
  $parsed{'country'}    = $lineage->{'country'}{'name'}  if exists
                          $lineage->{'country'}{'name'};

  # add a new key to store information about the existence of layers
  $parsed{'layers'}  = {'micro'   => 0,
                        'macro'   => exists $parsed{'neighbourhood'}    ||
                                     exists $parsed{'borough'}          ||
                                     exists $parsed{'locality'}         ||
                                     exists $parsed{'county'}           ||
                                     exists $parsed{'region'}           || 0,
                        'postal'  => exists $parsed{'postalcode'}    ? 1 : 0,
                        'country' => exists $parsed{'country'}       ? 1 : 0};

  return %parsed;
}

# =============================================================================
# abbreviate_address
# this is only a dummy implementation of abbreviate_address
# =============================================================================
sub abbreviate_address {
  my $address = shift;
  my %params  = @_;
  return expand_source_abbreviations($address,
                                     $params{'source'},
                                     $params{'regex'},
                                     $params{'value'},
                                     1,
                                     $params{'iso3'}
                                     );
}

# =============================================================================
# nerf_distance(x,y) = distance(p(x),p(y))
# p() = normalize + remove extensible tokens + remove reducible tokens + fingerprint
#
# nerf_distance(Lab Glaxo Smith Kline, Glaxo Smith Kine Incorporated)
#
# normalize(Lab Glaxo Smith Kline)                 =  lab glaxo smith kline
# normalize(Glaxo Smith Kine Incorporated)         =  glaxo smith kine incorporated
#
# remove_extensible(lab glaxo smithk line)         =  glaxo smith kline
# remove_extensible(glaxo smith kine incorporated) =  glaxo smith kine incorporated
#
# remove_reducible(glaxosmithkline)                =  glaxo smith kline
# remove_reducible(glaxosmithkine incorporated)    =  glaxo smith kine
#
# fingerprint(glaxo smith kline)                   =  glaxo kline smith
# fingerprint(glaxo smith kine)                    =  glaxo kine  smith
#
# distance(glaxo kline smith, glaxo kine smith)                      =
# compared to:
# distance(Lab Glaxo Smith Kline,  Glaxo Smith Kine Incorporated)    =
# =============================================================================
sub nerf_distance {
  my $iname  = shift;
  my $oname  = shift;
  my $iso3   = shift;
  my $source = shift;
  my $func   = shift;

  # if both strings are equal return 0
  return 0 if $iname eq $oname;

  # the distance to an empty string is 1
  return 1 if $iname eq "" or $oname eq "";

  # numbers will not be taken in account
  if (($iname =~ /\d/ and $oname !~ /\d/ ) or
      ($iname !~ /\d/ and $oname =~ /\d/ )) {
    $iname =~ s/\d//g;
    $oname =~ s/\d//g;
  }

  # normalize strings
  $iname = $Fingerprint::function{'normalize'}{'ref'}($iname);
  $oname = $Fingerprint::function{'normalize'}{'ref'}($oname);

  # if both normalized strings are now equal return 0
  return 0 if $iname eq $oname;

  # if we know the input country try to remove the extensible/reducible words
  if (defined $iso3) {
    # get the main lang code of the input country
    my $lang = $lr{'common'}{'country'}{'iso3'}{$iso3}{'lang'};

    # expand the input address
    my @iname_expand = expand_address($iname, languages  => [$lang]);

    # keep only the unextensible tokens
    $iname = $Fingerprint::function{'intersection'}{'ref'}($iname,$iname_expand[0]);

    # expand the output address
    my @oname_expand = expand_address($oname, languages  => [$lang]);

    # keep only the unextensible tokens
    $oname = $Fingerprint::function{'intersection'}{'ref'}($oname,$oname_expand[0]);

    # if both strings are now equal return 0
    return 0 if $iname eq $oname;

    # abbreviate the input address
    my @iname_abbreviate = abbreviate_address($iname, languages  => [$lang],
                                                      source => $source,
                                                      regex  => 'xeger',
                                                      value  => 'abbreviation',
                                                      iso3   => $iso3);

    # keep only the unreducible tokens
    $iname = $Fingerprint::function{'intersection'}{'ref'}($iname, $iname_abbreviate[0]);

    # abbreviate the output address
    my @oname_abbreviate = abbreviate_address($oname, languages  => [$lang],
                                                      source => $source,
                                                      regex  => 'xeger',
                                                      value  => 'abbreviation',
                                                      iso3   => $iso3);

    # keep only the unreducible tokens
    $oname = $Fingerprint::function{'intersection'}{'ref'}($oname, $oname_abbreviate[0]);

    # if both strings are now equal return 0
    return 0 if $iname eq $oname;
  }

  # this is based on the idea that the relevance of a place name word
  # decreases with respect to the position in which it is found inside
  # the whole place name
  my $longer  = length($oname) > length($iname) ? $oname : $iname;
  my $shorter = $longer eq $oname ? $iname : $oname;
  my $index = index($longer, $shorter);
  return $index/(length($longer)+length($shorter)) if $index != -1;

  # calculate the normalized fingerprint
  $iname = $Fingerprint::function{'sort_uniq'}{'ref'}($iname);
  $oname = $Fingerprint::function{'sort_uniq'}{'ref'}($oname);

  # if both strings are now equal return 0
  return 0 if $iname eq $oname;

  # calculate the distance of the normalized strings
  return $func->($iname, $oname);
}

# =============================================================================
# is_micro_part_of_macro()
# =============================================================================
sub is_micro_part_of_macro($$) {
 my $micro_feature = shift;
 my $macro_feature = shift;

 # $micro_feature is undef return 0
 return 0 if not defined $micro_feature;

 # $macro_feature is undef return 1
 return 1 if not defined $macro_feature;

 # $micro_feature and $micro_feature are the same return 1
 return 1 if $micro_feature == $macro_feature;

 # @see https://github.com/whosonfirst/whosonfirst-placetypes
 # the minimum list of place types for a hierarchy applied
 # globally looks like this:
 # - continent (C)
 #   - country (C)
 #     - region (C)
 #        - county (CO)
 #           - locality (C)
 #             - neighbourhood (C)

 my $macro_properties = $macro_feature->{'properties'};
 my $micro_properties = $micro_feature->{'properties'};

 # continent (C)
 if (scalar $macro_properties->{'continent'} and
     scalar $micro_properties->{'continent'} and
            $macro_properties->{'continent'} ne
            $micro_properties->{'continent'}) {
   return 0;
 }

 # country (C)
 if (scalar $macro_properties->{'country_a'} and
     scalar $micro_properties->{'country_a'} and
            $macro_properties->{'country_a'} ne
            $micro_properties->{'country_a'}) {
   return 0;
 }

 # region (C)
 if (scalar $macro_properties->{'region'} and
     scalar $micro_properties->{'region'} and
            $macro_properties->{'region'} ne
            $micro_properties->{'region'}) {
   return 0;
 }

 # locality (C)
 if (scalar $macro_properties->{'locality'} and
     scalar $micro_properties->{'locality'} and
            $macro_properties->{'locality'} ne
            $micro_properties->{'locality'}) {
   return 0;
 }

 # neighbourhood (C)
 if (scalar $macro_properties->{'neighbourhood'} and
     scalar $micro_properties->{'neighbourhood'} and
            $macro_properties->{'neighbourhood'} ne
            $micro_properties->{'neighbourhood'}) {
   return 0;
 }


 # micro feature needs to have a smaller kind (greater constant) of layer
 if ($layer_hierarchy{$micro_properties->{'layer'}} <
     $layer_hierarchy{$macro_properties->{'layer'}}) {
   if ($config->geocoding_mode eq 'macro-micro') {
     # always keep the smallest feature
     return 0;
   } elsif ($config->geocoding_mode eq 'macro-meso') {
     # only if we really have a meso layer
     # macro layer thinner than a micro level
     # micro layer greater than a macro level
     if ($layer_hierarchy{$macro_properties->{'layer'}} <
         $layer_levels{'micro'} ||
         $layer_hierarchy{$micro_properties->{'layer'}} <
         $layer_levels{'meso'}) {
       return 0;
     }
   }
 }

 return 1;
}

# =============================================================================
# expand_abbreviation()
# =============================================================================
sub expand_abbreviation {
 my $address = shift;
 my $dict    = shift;
 my $regex   = shift;
 my $value   = shift || 'meaning';
 my $supress = shift || 0;

 # regex substitution
 if (not $supress) {
   $address =~ s/\b($regex)\b/$dict->{$1}{$value}/g;
 } else {
   $address =~ s/\b($regex)\b//gi;
 }


 return $address;
}

# =============================================================================
# expand_source_abbreviations()
# TODO(you) train/use instead libpostal_expand
# @see https://github.com/openvenues/libpostal (resources/dictionaries)
# A more efficient implementation is possible using Unitex
# =============================================================================
sub expand_source_abbreviations {
 my $address = shift;
 my $source  = shift;
 my $regex   = shift || 'regex';
 my $value   = shift || 'meaning';
 my $supress = shift || 0;
 my $iso3    = shift || undef;

 # 1. common address fixes ('all')
 # This step must be done first in order to fix and normalize addresses
 if ( $source eq 'all' && exists $lr{$source}{'addresses'}{'common'} ) {
   $address = expand_abbreviation($address,
                                  $lr{$source}{'addresses'}{'common'}{'dict'},
                                  $lr{$source}{'addresses'}{'common'}{$regex},
                                  $value,
                                  0);
 }

 # if iso3 is not provided then try to guess the name of the country
 if (not defined $iso3) {
  (my $country) = $address =~ /\b($lr{'common'}{'country'}{'name_regex'})\.?$/;

  if (defined $country) {
   # capitalize the country name as the hashify function on setup_resources()
   # keeps for now the input as-is
   $country = $Fingerprint::function{'capitalize'}{'ref'}($country);
   # get the iso3 code of the country
   $iso3 = exists $lr{'common'}{'country'}{'name'}{$country} ?
                  $lr{'common'}{'country'}{'name'}{$country}{'iso3'}      : "";
  }
 }

 # 1. abbreviations that are only for an specific country
 if ( defined $iso3 and exists $lr{$source}{'countries'}{$iso3} ) {
   $address = expand_abbreviation($address,
                                  $lr{$source}{'countries'}{$iso3}{'dict'},
                                  $lr{$source}{'countries'}{$iso3}{$regex},
                                  $value,
                                  $supress);
 }

 # 2. abbreviations for 'all' countries
 if ( exists $lr{$source}{'countries'}{'all'} ) {
   $address = expand_abbreviation($address,
                                  $lr{$source}{'countries'}{'all'}{'dict'},
                                  $lr{$source}{'countries'}{'all'}{$regex},
                                  $value,
                                  $supress);
 }

 # 3. abbreviations that are only for an specific language
 # get the main lang code of the country
 my $lang = defined $iso3  ? $lr{'common'}{'country'}{'iso3'}{$iso3}{'lang'} : 'all';

 if ( exists $lr{$source}{'langs'}{$lang} ) {
  $address = expand_abbreviation($address,
                                 $lr{$source}{'langs'}{$lang}{'dict'},
                                 $lr{$source}{'langs'}{$lang}{$regex},
                                 $value,
                                 $supress);
 }

 # 4. common address abbreviations (not 'all')
 if ( $source ne 'all' && exists $lr{$source}{'addresses'}{'common'} ) {
   $address = expand_abbreviation($address,
                                  $lr{$source}{'addresses'}{'common'}{'dict'},
                                  $lr{$source}{'addresses'}{'common'}{$regex},
                                  $value,
                                  $supress);
 }

 return $address;
}

# =============================================================================
# on_progress_update()
# =============================================================================
sub on_progress_update {
 # early return wheneaver there are nothing to do
 return if not $config->progress_file;

 my $name   = shift;
 my $strPC  = shift;
 my $strETA = shift;
 my $self   = shift;

 # create a YAML string
 my $yaml   = sprintf("name     : %s\nprogress : %s\neta      : %s\n",
                      $name, $strPC, $strETA);

 # truncate and write the YAML string
 truncate $progress_handler, 0;
 seek $progress_handler, 0, 0;
 print $progress_handler $yaml;
}

# =============================================================================
# get_geo_region()
# =============================================================================
sub get_geo_region($$) {
  my $micro_feature = shift;
  my $macro_feature = shift;

  # on vanilla mode return directly the locality attribute
  if ($config->geocoding_mode eq 'vanilla' ||
      $config->geocoding_mode eq 'norm-code') {
   return $micro_feature->{'properties'}->{'region'};
  }

  # select a region or a macroregion
  return scalar $micro_feature->{'properties'}->{'region'}      ?
                $micro_feature->{'properties'}->{'region'}      :
         scalar $macro_feature->{'properties'}->{'region'}      ?
                $macro_feature->{'properties'}->{'region'}      :
         scalar $micro_feature->{'properties'}->{'macroregion'} ?
                $micro_feature->{'properties'}->{'macroregion'} :
         scalar $macro_feature->{'properties'}->{'macroregion'} ?
                $macro_feature->{'properties'}->{'macroregion'} : '';
}

# =============================================================================
# get_geo_locality()
# =============================================================================
sub get_geo_locality($$) {
  my $micro_feature = shift;
  my $macro_feature = shift;

  # on vanilla mode return directly the locality if exist, if not return localadmin
  if ($config->geocoding_mode eq 'vanilla' ||
      $config->geocoding_mode eq 'norm-code') {
   if ($config->geocoding_mode eq 'vanilla' ||
       $config->geocoding_mode eq 'norm-code') {                                                                                                                         
    if ($micro_feature->{'properties'}->{'locality'}){                                                                                                                
      return $micro_feature->{'properties'}->{'locality'};                                                                                                          
    } elsif ($micro_feature->{'properties'}->{'localadmin'}) {                                                                                                        
       return $micro_feature->{'properties'}->{'localadmin'};
    } elsif ($micro_feature->{'properties'}->{'neighbourhood'}) {
        return $micro_feature->{'properties'}->{'neighbourhood'};    
    } elsif ($micro_feature->{'properties'}->{'county'}) {
        return $micro_feature->{'properties'}->{'county'}; 
    }
   }
  }

  # select a locality or a localadmin
  if(scalar $micro_feature->{'properties'}->{'country_a'} and
            $micro_feature->{'properties'}->{'country_a'} ne 'USA') {
    return scalar $micro_feature->{'properties'}->{'locality'}   ?
                  $micro_feature->{'properties'}->{'locality'}   :
           scalar $macro_feature->{'properties'}->{'locality'}   ?
                  $macro_feature->{'properties'}->{'locality'}   :
           scalar $micro_feature->{'properties'}->{'localadmin'} ?
                  $micro_feature->{'properties'}->{'localadmin'} :
           scalar $macro_feature->{'properties'}->{'localadmin'} ?
                  $macro_feature->{'properties'}->{'localadmin'} : '';
  } else {
    return scalar $micro_feature->{'properties'}->{'localadmin'} ?
                  $micro_feature->{'properties'}->{'localadmin'} :
           scalar $macro_feature->{'properties'}->{'localadmin'} ?
                  $macro_feature->{'properties'}->{'localadmin'} :
           scalar $micro_feature->{'properties'}->{'locality'}   ?
                  $micro_feature->{'properties'}->{'locality'}   :
           scalar $macro_feature->{'properties'}->{'locality'}   ?
                  $macro_feature->{'properties'}->{'locality'}   : '';
  }
}

# =============================================================================
# get_geo_label()
# =============================================================================
sub get_geo_label($) {
    my $micro_feature = shift;
    
    if (scalar $micro_feature->{'properties'}->{'label'}) {
        $micro_feature->{'properties'}->{'label'} =~ s/"//g;
    }
    
    return $micro_feature->{'properties'}->{'label'};
}

# =============================================================================
# get_geo_iso2()
# =============================================================================
sub get_geo_iso2($) {
  my $feature = shift;
  my $iso2 = '';
  if (scalar $feature->{'properties'}->{'country_a'}) {
    $iso2 = country_code2code($feature->{'properties'}->{'country_a'}, LOCALE_CODE_ALPHA_3, LOCALE_CODE_ALPHA_2);
    # UPPERCASE using uc() function
    $iso2 = uc($iso2);
  }
  return $iso2;
}

# =============================================================================
# clean_country_aliases()
# =============================================================================
sub clean_country_aliases {
    my $line = shift;
   
    # Replace country aliases if they exist within the address by their official name
    $line = replace_string($line, 'Afghanistan' ,'Afuhan');
    $line = replace_string($line, 'Bangladesh' ,'Bengal');
    $line = replace_string($line, 'Belgium' ,'Flanders');
    $line = replace_string($line, 'Belize' ,'British Honduras');
    $line = replace_string($line, 'Cambodia' ,'Kampuchia');
    $line = replace_string($line, 'Cambodia' ,'Kambuja');
    $line = replace_string($line, 'Canada' ,'New France');
    $line = replace_string($line, 'China' ,'Cathay');
    $line = replace_string($line, 'China' ,'Catay');
    $line = replace_string($line, 'China' ,'Katai');
    $line = replace_string($line, 'China' ,'Zhonghua');
    $line = replace_string($line, 'China' ,'Huaxia');
    $line = replace_string($line, 'China' ,'Shenzhou');
    $line = replace_string($line, 'China' ,'Jiuzho');
    $line = replace_string($line, 'China' ,"People's Republic of China");
    $line = replace_string($line, 'China' ,'Kara');
    $line = replace_string($line, 'China' ,'Peoples R China');
    $line = replace_string($line, 'Finland' ,'Finrando'); 
    $line = replace_string($line, 'France' ,'French Republic');    
    $line = replace_string($line, 'Germany' ,'Greater Prussia');
    $line = replace_string($line, 'Germany' ,'Federal Republic of Germany');
    $line = replace_string($line, 'Germany' ,'BRD');
    $line = replace_string($line, 'Germany' ,'FRG');
    $line = replace_string($line, 'Germany' ,'German Democratic Republic');
    $line = replace_string($line, 'Germany' ,'DDR');
    $line = replace_string($line, 'Germany' ,'GDR');
    $line = replace_string($line, 'Iran' ,'Persia'); 
    $line = replace_string($line, 'Japan' ,'Nihon');
    $line = replace_string($line, 'South Korea' ,'Joseon');
    $line = replace_string($line, 'South Korea' ,'Hanhunk');
    $line = replace_string($line, 'South Korea' ,'Choson'); 
    $line = replace_string($line, 'South Korea' ,'Republica de Corea'); 
    $line = replace_string($line, 'Netherlands' ,'Holland');
    $line = replace_string($line, 'Netherlands' ,'Frisia');
    $line = replace_string($line, 'Netherlands' ,'Friesland');
    $line = replace_string($line, 'Sri Lanka' ,'Celyon');
    $line = replace_string($line, 'Thailand' ,'Siam');
    $line = replace_string($line, 'Vietnam' ,'Nam viet');
    $line = replace_string($line, 'Yemen' ,'Sheba');
    $line = replace_string($line, 'United Arab Emirates' ,'U Arab Emirates');
    $line = replace_string($line, 'United Kingdom' ,'United Kingdom of Great Britain and Northern Ireland (the)'); 
    $line = replace_string($line, 'United Kingdom' ,'United Kingdom of Great Britain and Northern Ireland'); 
    $line = replace_string($line, 'United Kingdom' ,'The United Kingdom of Great Britain and Northern Ireland'); 
    
    return $line;
}

sub clean_hk {
    my $line = shift;
    
    my $hongkong = 'Hong Kong, Hong Kong'; 
    my $taiwan = 'Taiwan';

    $line = replace_string($line, $hongkong, 'Hong Kong CN, China');
    $line = replace_string($line, $hongkong, 'Hong Kong CN'); 
    $line = replace_string($line, $hongkong, 'Hong Kong, China'); 
    $line = replace_string($line, $hongkong, 'HK, China');
    $line = replace_string($line, $hongkong, 'Hong Kong, CN');
    $line = replace_string($line, $hongkong, 'Hong Kong City, China');
    $line = replace_string($line, $hongkong, 'Hong Kong City');
    $line = replace_string($line, $hongkong, 'Hong Kong Special Administration Region, China');
    $line = replace_string($line, $hongkong, 'Hong Kong Special Administration Region');
    $line = replace_string($line, $hongkong, 'Hong Kong Special Administration Region, CN');
    $line = replace_string($line, $hongkong, 'Hong Kong SAR, China');
    $line = replace_string($line, $hongkong, 'Hong Kong SAR');
  
    $line = replace_string($line, $taiwan, 'Taiwan, China');
    $line = replace_string($line, $taiwan, 'Taiwan, CN');
    $line = replace_string($line, $taiwan, 'Taiwan , CN');
    $line = replace_string($line, $taiwan, 'Taiwan CN');
   
    return $line;
}

sub clean_GB {
    my $line = shift;
    $line =~ /(\b(\w|[-])*\s*$)/; 

    if (rindex($line, 'England') != -1) {
        $line = replace_string($line, '', 'United Kingdom');
    } elsif (rindex($line, 'England') == -1) {
        $line = $line; 
    }

    return $line;
}

# =============================================================================
# preprocessing_addresses()
# =============================================================================
sub preprocessing_addresses {
    my $line = shift;

    # Replace country aliases with official names
    $line = clean_country_aliases($line);
    $line = clean_hk($line);
    #$line = clean_GB($line);

    $line = replace_string($line, ',', ';');
    $line = replace_string($line, ' ', '- ');
    
    $line = replace_string($line, ' ', ' - ');
    $line = replace_string($line, ' ', ' -');
    $line = replace_string($line, ' ', '|');
    $line = replace_string($line, ' ', '/');
  
    # Remove multiple commas
    $line =~ s/,+/,/g;
    
    # Trim dots, commas and whitespaces from the country name
    $line =~ s/^\.+|\.+$//g;
    $line =~ s/^\,+|\,+$//g;
    $line =~ s/^\s+|\s+$//g;

    # Add space if lowercase and uppercase are consecutive
    $line =~ s/(?<! )(?<!^)(?<![A-Z0-9])[A-Z0-9]/ $&/g;

    # Adding space after punctaction symbol 
    $line =~ s/(\d[,.]\d)|([^\w\s-](?!\s))/$1 || "$2 "/ge;
 
    return $line;
}

# =============================================================================
# replace_string()
# =============================================================================
sub replace_string {
    my $string = shift;
    my $replace = shift;
    my $find = shift;

    $string =~ s/\Q$find\E/$replace/g;
    return $string;
}

# =============================================================================
# format_addr()
# =============================================================================
sub format_addr {
    my $address = shift;
    my $ctry_code = shift;   
    my @ctry_postcode = ['at','au','ca','ch','fi','fr','gb','jp','li','nl','us'];
    my $cedex;

    # If Cedex exist within address then remove it 
    if($address =~ m/Cedex/){
        $cedex = 'true';
        $address =~ s/Cedex *[0-9]{1,3}//g;
    }

    my %parsed_address = parse_libpostal($address);

    # List of keys to consider in order to format the new address 
    my @keys = qw/postcode suburb city_district city island state_district state country country_region world_region/;
    my %new_hash; 
    my $key_no_postal;  

    @new_hash{@keys} = @parsed_address{@keys};
    
    my $size = 0;
    my $formatted_addr = '';    
    my $formatted_no_postal = '';

    foreach my $key (@keys) {
        if (defined $new_hash{$key}) 
        {
            if ($key eq 'city') { $key =~ s/\d//g; }
            $key_no_postal = $new_hash{$key};
            if ($key eq 'postcode'){
                $key_no_postal = '';
               if( $ctry_code ~~ @ctry_postcode ) { 
                    if ($cedex) {
                        $new_hash{$key} = '';
                    } else {
                        $new_hash{$key} =~ s/[a-zA-Z]*-//g;
                    }
                } else {
                    $new_hash{$key} = '';
                    $size--;
                }    
            }
            $formatted_no_postal = $formatted_no_postal . $key_no_postal . ", ";
            $formatted_addr = $formatted_addr . $new_hash{$key} . ", "; 
            $size++;
        }
    }

    if($size < 2 and defined $new_hash{'country'}){
        $formatted_addr = ''; 
    }

    # Delete remaining commas and spaces
    $formatted_addr =~ s/^\,+|\,+$//g;
    $formatted_addr =~ s/^\s+|\s+$//g;
    $formatted_addr =~ s/^\,+|\,+$//g;

    $formatted_no_postal =~ s/^\,+|\,+$//g;
    $formatted_no_postal =~ s/^\s+|\s+$//g;
    $formatted_no_postal =~ s/^\,+|\,+$//g;

    return ($formatted_addr, $formatted_no_postal);
}

sub clean_postalcode {
    my $address = shift;
    my $country = shift;
    my %country_regex = (
        "az" => '\b((AZ[-]?\s?)?([0-9]{4}|[0-9]{6}))\b',
        "sv" => '\b(CP\s?)?([0-9]{4})\b',
        "cu" => '\b(CP\s?)?([0-9]{5})\b',
        "ht" => '\b(HT)?([0-9]{4})\b',
        "lv" => '\b(LV[- ]*)?([0-9]{4})\b',
        "kr" => '\b(?:SEOUL\s?)?([0-9]{3}-?\s?[0-9]{3})\b',
        "pt" => '\b([0-9]{3,4})-\s?[0-9]{3}\b',
        "in" => '\b([0-9]{3}\s?[0-9]{3})\b',
        "re" => '\b((97|98)(4|7|8)[0-9]{2})\b',
        "gp" => '\b((97|98)[0-9]{3})\b',
        "ir" => '\b([0-9]{5}-?\s?[0-9]{5})\b',
        "ls" => '\b([0-9]{3})\b',
        "mg" => '\b([0-9]{3})\b',
        "om" => '\b([0-9]{3})\b',
        "pg" => '\b([0-9]{3})\b',
        "bh" => '\b([0-9]{3}[0-9]?)\b',
        "lb" => '\b([0-9]{4}([0-9]{4})?)\b',
        "al" => '\b(AL-\s?)?([0-9]{4})\b',
        "bd" => '\b([0-9]{4})\b',
        "bg" => '\b([0-9]{4})\b',
        "cv" => '\b([0-9]{4})\b',
        "cy" => '\b([0-9]{4})\b',
        "et" => '\b([0-9]{4})\b',
        "ge" => '\b([0-9]{4})\b',
        "gw" => '\b([0-9]{4})\b',
        "lr" => '\b([0-9]{4})\b',
        "mk" => '\b([0-9]{4})\b',
        "mz" => '\b([0-9]{4})\b',
        "ne" => '\b([0-9]{4})\b',
        "nz" => '\b([0-9]{4})\b',
        "ph" => '\b([0-9]{4})\b',
        "py" => '\b([0-9]{4})\b',
        "tn" => '\b([0-9]{4})\b',
        "ve" => '\b([0-9]{4})\b',
        "za" => '\b([0-9]{4})\b',
        "ba" => '\b([0-9]{5})\b',
        "cr" => '\b([0-9]{5})\b',
        "cs" => '\b([0-9]{5})\b',
        "do" => '\b([0-9]{5})\b',
        "dz" => '\b([0-9]{5})\b',
        "ee" => '\b([0-9]{5})\b',
        "eg" => '\b([0-9]{5})\b',
        "gt" => '\b([0-9]{5})\b',
        "id" => '\b([0-9]{5})\b',
        "il" => '\b((IL- |I- )?([0-9]{7}|[0-9]{5}))\b',
        "iq" => '\b([0-9]{5})\b',
        "jo" => '\b([0-9]{5})\b',
        "ke" => '\b([0-9]{5})\b',
        "kh" => '\b([0-9]{5})\b',
        "kw" => '\b([0-9]{5})\b',
        "la" => '\b([0-9]{5})\b',
        "lk" => '\b([0-9]{5})\b',
        "ma" => '\b([0-9]{5})\b',
        "me" => '\b([0-9]{5})\b',
        "mm" => '\b([0-9]{5})\b',
        "mq" => '\b([0-9]{5})\b',
        "mv" => '\b([0-9]{5})\b',
        "mx" => '\b(C[.]?\s?P[.]?\s?)?([0-9]{5})\b',
        "my" => '\b([0-9]{5})\b',
        "np" => '\b([0-9]{5})\b',
        "pk" => '\b([0-9]{5})\b',
        "sa" => '\b([0-9]{5})\b',
        "sd" => '\b([0-9]{5})\b',
        "sn" => '\b([0-9]{5})\b',
        "th" => '\b([0-9]{5})\b',
        "tr" => '\b([0-9]{5})\b',
        "tw" => '\b([0-9]{5})\b',
        "ua" => '\b([0-9]{5})\b',
        "uy" => '\b([0-9]{5})\b',
        "zm" => '\b([0-9]{5})\b',
        "am" => '\b([0-9]{6})\b',
        "by" => '\b([0-9]{6})\b',
        "cn" => '\b([0-9]{6})\b',
        "co" => '\b([0-9]{6})\b',
        "kg" => '\b([0-9]{6})\b',
        "kp" => '\b([0-9]{6})\b',
        "kz" => '\b([0-9]{6})\b',
        "mn" => '\b([0-9]{6})\b',
        "ng" => '\b([0-9]{6})\b',
        "ro" => '\b([0-9]{6})\b',
        "rs" => '\b([0-9]{6})\b',
        "ru" => '\b([0-9]{6})\b',
        "sg" => '\b([0-9]{6})\b',
        "tj" => '\b([0-9]{6})\b',
        "tm" => '\b([0-9]{6})\b',
        "uz" => '\b([0-9]{6})\b',
        "vn" => '\b([0-9]{6})\b',
        "cl" => '\b([0-9]{7})\b',
        "ni" => '\b([0-9]{5}|[0-9]{7})\b',
        "br" => '\b((CEP[:-]?\s?)|(BR-\s?))?([0-9]{5}[-]?\s?[0-9]{3})\b',
        "sz" => '\b([A-Z][0-9]{3})\b',
        "bn" => '\b([A-Z]{2}\s?[0-9]{4})\b',
        "hn" => '\b([A-Z]{2}[0-9]{4})\b',
        "so" => '\b([A-Z]{2}[0-9]{5})\b',
        "ec" => '\b([a-zA-Z][0-9]{4}[a-zA-Z])\b',
        "dk" => '\b(([D-d][K-k]( |-|- )?)?[1-9]{1}[0-9]{3})|([0-9]{4})\b',
        "be" => '\b(BE- |B- )?[1-9]{1}[0-9]{3}\b',
        "de" => '\b(DE- |DE|D- )?((?:0[1-46-9]\d{3})|(?:[1-357-9]\d{4})|(?:[4][0-24-9]\d{3})|(?:[6][013-9]\d{3}))\b',
        "es" => '\b(E- )?([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}\b',
        "gr" => '\b(GR- |GR )?([0-9]{5}|[0-9]{2}( |-)[0-9]{3}|[0-9]{3}( |-)[0-9]{2})\b',
        "hr" => '\b(HR- |HR )?[0-9]{5}\b',
        "hu" => '\b(HU- |HU |H- )?[0-9]{4}\b',
        "is" => '\b(IS- )?[0-9]{3}\b',
        "it" => '\b(IT- |V- |I- )?[0-9]{5}\b',
        "lt" => '\b(LT-? )?([0-9]{4,5})\b',
        "lu" => '\b(LU- |LU |L- )?[0-9]{4}\b',
        "mc" => '\b(MC- |MC )?[0-9]{5}\b',
        "no" => '\b(NO- |NO |A- |N- )?[0-9]{4}\b',
        "pl" => '\b(PL- |PL )?([(0-9]{2})-([(0-9]{3})\b',
        "se" => '\b((SE|s|S)-?\s?)?[0-9]{3}\s?[0-9]{2}\b',
        "si" => '\b(SI- |SI )?[0-9]{4}\b',
        "sk" => '\b(SK- |SK )?[0-9]{3}\s?[0-9]{2}\b',
        "cz" => '\b[0-9]{3}\s?[0-9]{2}\b',
        "ar" => '\b[A-Z]?[0-9]{4}[A-Z]{0,3}\b',
        "ie" => '\b([A-Z][0-9]{2})|([A-Z]{7})|((D6W|[AC-FHKNPRTV-Y][0-9]{2})\s?([AC-FHKNPRTV-Y0-9]{4}))\b',
        "mt" => '\b[A-Z]{3}\s?[0-9]{4}\b',
        "pr" => '\b00[679][0-9]{2}(?:-[0-9]{4})?\b',
        "md" => '\bM[0-9D]-\s?[0-9]{4}\b');
        
    if (defined $country){
        if (exists $country_regex{$country}){ 
            $address =~ s/$country_regex{$country}//g;
        }
    }
    return $address;
}

# =============================================================================
# country_boundary()
# =============================================================================
sub clean_china {
    my $address = shift;
    my $country = shift;

    $country = lc($country);
    if($country eq 'taiwan' || $country eq 'hong kong') {
        $address = replace_string($address, '' ,', CN');
        $address = replace_string($address, '' ,', China');
    }
    # Return the iso code that delimiter the country boundary as geocoding parameter
    return $address;
}

# =============================================================================
# address_iso2country()
# =============================================================================
sub address_iso2country {
    my $address = shift;
    my %countries;
    my $iso_ctry;

    my @codes = all_country_codes();
    my @xsplit = split /,/, $address;
    my $i = scalar @xsplit - 1;
    $iso_ctry = lc $xsplit[$#xsplit];
    $iso_ctry =~ s/^\s+//;
    
    if (grep { $_ eq $iso_ctry } @codes){
      $xsplit[$i] = code2country($iso_ctry, 'alpha-2');
      my $xjoin = join(",", @xsplit);
      $address = $xjoin;
    }
    return $address;
}

# =============================================================================
# country_boundary()
# =============================================================================
sub country_boundary {
    my $address = shift;
    $address = address_iso2country($address);
    $address = replace_string($address, 'United Kingdom' ,'England');
    
    my %parsed_address = parse_libpostal($address);
    my $country_boundary; 

    if ($parsed_address{'country'}){  
        $parsed_address{'country'} =~ s/[\$#@~!&*()\[\];.,:?^`\\\/]+//g;
        $country_boundary = country2code($parsed_address{'country'}, 'alpha-2');
    } elsif ($parsed_address{'state'}){ 
        $parsed_address{'state'} =~ s/[\$#@~!&*()\[\];.,:?^ `\\\/]+//g;
        $country_boundary = country2code($parsed_address{'state'}, 'alpha-2');
    } 
 
    # Return the iso code that delimiter the country boundary as geocoding parameter
    return $country_boundary;
}

# =============================================================================
# parse_libpostal()
# =============================================================================
sub parse_libpostal {
    my $address = shift;
    my %parsed_address;
    $address = ($address eq "" or not defined $address) ? 'NaN' : $address;

    try {
        %parsed_address = parse_address($address);
    } catch {
        warn "caught error: ";
    };

    # Return libpostal parsed_address
    return %parsed_address;
}


# =============================================================================
# csv_worker()
# =============================================================================
sub csv_worker($) {
  my $total_of_records = shift;

  # continue only if there are records to process
  return 1 if $total_of_records <= 0;

  # simple progress bar on the terminal
  my $progress = Term::ProgressiveBar->new({name      => 'geocoding',
                                            count     => $total_of_records,
                                            silent    => $config->progress_bar eq 'off',
                                            remove    => 1,
                                            fh        => \*STDERR,
                                            ETA       => 'linear',
                                            on_update => \&on_progress_update,});

  # This value is taken as being the maximum speed between updates
  # to aim for. It is only meaningful if ETA is switched on
  $progress->max_update_rate(1);

  # switches off the use of minor characters
  $progress->minor(0);

  # to only call $progress->update again when needed
  my $next_update = 0;

  # to keep track the number of addresses processed
  my $count = 0;

  # to
  my $threshold_confidence = $config->threshold_confidence;

  # global benchmark starts
  $benchmark{'global'}{'t_start'} = Benchmark->new if
                                    $config->benchmark_global eq 'on';

  # loop through each csv file
  while (my $file = shift @ARGV and
         $count < $total_of_records) {
    # try to open
    sysopen(INPUT, $file, O_RDONLY) or error("Could not open csv file: '%s'", $file);

    # get the csv header
    my @input_columns = $csv_in->header(*INPUT);

    # turn the columns array into a hash
    my %input_column = map { $_ => 1 } @input_columns;

    # check if the column 'address' exists
    exists $input_column{$config->csv_in_address} or error("Invalid CSV file, column 'address' alias '%s' not found on file: '%s'", $config->csv_in_address, $file);
    # info("Invalid CSV file, column 'address' alias '%s' not found on file: '%s'", $config->csv_in_address, $file);

    # push the address column name
    # $address_column{'address'} = 1;
    $address_column{$config->csv_in_address} = 1;

    # if the csv is already parsed, push the extra column names
    if ( $config->csv_in_parsed ) {
      $address_column{$config->csv_in_postalcode} = exists $input_column{$config->csv_in_postalcode};
      $address_column{$config->csv_in_city}       = exists $input_column{$config->csv_in_city};
      $address_column{$config->csv_in_country}    = exists $input_column{$config->csv_in_country};
    }

    # base query
    my $base = {};

    # layer
    if ($config->_exists('search_layer')) {
      $base->{'layers'} = $config->search_layers;
    }

    # size
    if ($config->_exists('search_size')) {
      $base->{'size'} = $config->search_size;
    }

    # only once
    if ($count == 0) {
     # get input column names
     foreach my $column_name (reverse(@input_columns)) {
       unshift @{$output_columns}, $column_name;
     }

     # combine and write out the column names as a string
     $csv_out->print ($output_handler, $output_columns);
    }

    # loop through rows
    while ( my $row = $csv_in->getline_hr( *INPUT ) and
            $count < $total_of_records) {
      # by default, use the whole 'address' column
      my $address = $row->{$config->csv_in_address};

      # if requested restructure the address
      if ( $config->csv_in_parsed ) {
        $address = $address . sprintf(",%s%s, %s",
          $address_column{$config->csv_in_postalcode} ? ' ' .
          $Fingerprint::function{'field_trim'}{'ref'}($row->{$config->csv_in_postalcode},',',' ') : '',
          $address_column{$config->csv_in_city}       ? ' ' .
          $Fingerprint::function{'field_trim'}{'ref'}($row->{$config->csv_in_city},',',' ') : '',
          $address_column{$config->csv_in_country} ?
          $Fingerprint::function{'field_trim'}{'ref'}($row->{$config->csv_in_country},',',' ') : '');
      }

      # trim address
      $address = $Fingerprint::function{'trim'}{'ref'}($address);

      # if address is empty do nothing
      goto print_result if $address eq "";

      # expand abbreviations
      if ($config->geocoding_mode ne 'vanilla' and
          $config->geocoding_mode ne 'norm-code') {
        $address = expand_source_abbreviations($address, 'all');
        $address = expand_source_abbreviations($address, $config->geocoding_source);
      }

      # prepare query
      my %query      = (%$base);
      $query{'text'} = $address;

      # to keep the parsed address
      my %parsed = ();

      # split the address at every colon
      my @parts = split / *, */, $address;
      my $j = scalar @parts - 1;
      my $i = $j - 1;

      # macro/meso address
      my $macro_meso_address = $j > 1 ? join(', ', @parts[$i..$j]) : $address;

      # macro or meso geographic feature
      my $macro_feature = undef;

      # macro, meso or micro geographic feature
      my $feature = undef;

      # macro-micro or macro-meso mode
      if ($config->geocoding_mode eq 'macro-micro' or
          $config->geocoding_mode eq 'macro-meso') {

        # try to parse the address using only the two trailing fields
        %parsed =  parse_address_convert($macro_meso_address);

        # awful trick
        my $placeholder_used = 0;
        my $parse_parts_used = 0;
        no_placeholder_used:

        # if there is not a macro layer then try to use the
        # placeholder service
        if (not exists $parsed{'layers'} or $parsed{'layers'}{'micro'}) {
          # set the fact that the placeholder service was used
          $placeholder_used = 1;

          # create a copy of the query
          my %placeholder_query = %query;

          # prepare the placeholder query
          $placeholder_query{'text'} = join(', ', @parts[$i..$j]) if $j > 1;

          # set the query components for the placeholder service
          $uri_placeholder->query_form(\%placeholder_query);

          # query the placeholder 'search' endpoint
          my $response = $ua_get->( $uri_placeholder );

          # placeholder response
          my $lineage = undef;

          if ($response->is_success) {
           $lineage = $response->json_content->[0]{'lineage'}[0];
           if (not defined $lineage) {
             $parsed{'layers'}{'micro'} = 1;
             goto no_parse_parts_used;
           }
           %parsed = parse_lineage_convert($lineage);
           if (not ($parsed{'layers'}{'macro'}  or
                    $parsed{'layers'}{'postal'} or
                    $parsed{'layers'}{'country'})) {
            goto no_parse_parts_used;
           }
          } else {
           # first raise a warning
           warning("%s %s", $response->code, $uri_placeholder->path_query);

           no_parse_parts_used:
           # set the fact that the parse parts code was used
           $parse_parts_used = 1;

           # and at last resort try to take portions of the address
           # until to found a macro layer or a country
           my $k = 1;

           # try to parse each @parts[$i..$j] sequence
           while ($k <= $j && $parsed{'layers'}{'micro'}) {
             %parsed  = parse_address_convert(join(', ', @parts[$k..$j]));
             $k++;
           }
          }
        }

        # fill all the params to perform the structured query
        # the structured query has the same params labels than
        # the parsed hash without the 'layers' key
        delete $parsed{'layers'};

        # only use the first part of the address
        $parsed{'address'}  = (split / *, */, $address)[0];

        my $reparsed_used = 0;
        no_reparsed_used:

        # sets and returns query components that use the
        # application/x-www-form-urlencoded format
        $uri_pelias_structured->query_form(\%parsed);

        my $response = $ua_get->( $uri_pelias_structured );
        
        if (not $response->is_success) {
          warning("%s %s", $response->code, $uri_pelias_structured->path_query);
        } else {
         # get the first macro feature and associate some extra params to
         # the micro query
         $macro_feature = $response->json_content->{'features'}[0];

         # if we do not have an answer
         if (not defined $macro_feature) {
           # try first to use the placeholder service
           if (not $placeholder_used) {
             goto no_placeholder_used;
           }
           # there is not anything else to try, no macro feature will be used
           goto no_macro_meso_used;
         }

         # try to reparse using only the postal code if the confidence
         # is lower than 0.5
         if (exists $macro_feature->{'properties'}->{'confidence'}  and
                     $macro_feature->{'properties'}->{'confidence'} < 0.5 and
             exists $parsed{'postalcode'} and
             exists $parsed{'locality'}  and
             not $reparsed_used) {
           delete $parsed{'locality'};
           # set the fact that the reparsing was used
           $reparsed_used = 1;
           goto no_reparsed_used;
         }

         # try to force the use of the placeholder service if
         # it was not possible to do better than get a country
         if (( not exists $macro_feature->{'properties'}->{'layer'} or
                          $macro_feature->{'properties'}->{'layer'} eq 'country' or
                          $macro_feature->{'properties'}->{'confidence'} < 0.5) and
             not $placeholder_used) {
          goto no_placeholder_used;
         }

         # if we're on the macro-micro mode and while looking for a macro feature
         # we finish for found a micro one, then we have already the final feature
         if ($config->geocoding_mode eq 'macro-micro' and
             exists $macro_feature->{'properties'}->{'layer'} and
             exists $macro_feature->{'properties'}->{'match_mode'} and
             $macro_feature->{'properties'}->{'match_type'} eq 'exact' and
             $layer_hierarchy{$macro_feature->{'properties'}->{'layer'}} >=
             $layer_levels{'micro'} ) {
              # we have already the final feature
              $feature = $macro_feature;
              goto micro_feature_found;
         }

         # use the macro features to narrow the search
         if (scalar $macro_feature->{'properties'}->{'country_a'}) {
          $query{'boundary.country'} = $macro_feature->{'properties'}->{'country_a'};
          if ($macro_feature->{'properties'}->{'confidence'} > 0.2) {
            if (not ($macro_feature->{'geometry'}->{'coordinates'}[0] == 0 &&
                     $macro_feature->{'geometry'}->{'coordinates'}[1] == 0)) {
             $query{'focus.point.lat'}  = $macro_feature->{'geometry'}->{'coordinates'}[0];
             $query{'focus.point.lon'}  = $macro_feature->{'geometry'}->{'coordinates'}[1];
            }
          }
         }
        }
      }

      my $macro_meso_used = 0;
      my $try_macro_meso  = 0;
      no_macro_meso_used:
      # 'macro-meso' mode: only macro or meso features
      if ($config->geocoding_mode eq 'macro-meso' or $try_macro_meso) {
        # set 'macro-meso' mode as used
        $macro_meso_used = 1;
        # flag to set if we want to use the coarse layer
        my $use_coarse_layer = 0;

        # if there is more than a single parsed field,
        # then use postalcode, locality and country as address
        if ($j > 1) {
          $query{'text'} = join(', ', @parts[$i..$j]);
          $use_coarse_layer = 1;
        }
        elsif (defined $macro_feature and
                            scalar $macro_feature->{'properties'}->{'locality'}) {
          $query{'text'}  = scalar $macro_feature->{'properties'}->{'postalcode'} ?
                                   $macro_feature->{'properties'}->{'postalcode'} . ", " :
                            scalar $parsed{'postalcode'} ?
                                   $parsed{'postalcode'} . ", " : "";
          $query{'text'} .= scalar $macro_feature->{'properties'}->{'locality'} ?
                                   $macro_feature->{'properties'}->{'locality'} . ", " : "";
          $query{'text'} .= scalar $macro_feature->{'properties'}->{'country'} ?
                                   $macro_feature->{'properties'}->{'country'} : "";
          $use_coarse_layer = 1;
        }
        elsif (scalar $parsed{'locality'}) {
          $query{'text'}  = scalar $parsed{'postalcode'} ?
                                   $parsed{'postalcode'} . ", " : "";
          $query{'text'} .= scalar $parsed{'locality'}   ?
                                   $parsed{'locality'}   . ", " : "";
          $query{'text'} .= scalar $parsed{'country'}    ?
                                   $parsed{'country'}    : "";
          $use_coarse_layer = 1;
        }

        # use the coarse layer whenever the address has only macro/meso-features
        if ($use_coarse_layer) {
         # 'coarse' is an alias for simultaneously using all administrative
         # layers, i.e. everything except 'venue' and 'address'
         $query{'layers'} = 'coarse';
        }
      }

      # if there is only a single parsed field identified as a country,
      # try only to get a country layer. Take notice that %parsed
      # contains at least one field: $parsed{'address'}
      if (($config->geocoding_mode ne 'vanilla' and
           $config->geocoding_mode ne 'norm-code') and
           scalar $parsed{'country'} and keys %parsed == 2) {
        # we force to ignore any macro feature
        undef $macro_feature;
        # only the country name
        $query{'text'} =  $parsed{'country'};
        # only a country layer
        $query{'layers'} = 'country';
      }
      
      my $call_pelias = 1;

      if ($config->geocoding_mode eq 'norm-code'){
        
        my $formatted_addr = '';
        my $formatted_no_postal = '';
        my @ctry_postcode = ['at','au','ca','ch','fi','fr','gb','jp','li','nl','us'];
        my $boundary = '';
        my $pre_addr;
        my %parsed_addr;
        my $response;
        my $call_layers = 0;
        my $check_response = 1;

        # $address = ($address eq "" or not defined $address) ? 'NaN' : $address;
        # pre_addr contains a clearer address, mainly formatting the punctaction symbols
        $pre_addr = preprocessing_addresses($address);
        # $pre_addr = ($pre_addr eq "" or not defined $pre_addr) ? $address : $pre_addr;
        %parsed_addr = parse_libpostal($pre_addr);

        if($parsed_addr{'country'}){
            $pre_addr = clean_china($pre_addr, $parsed_addr{'country'}); 
        }

        # %parsed_addr = parse_address($pre_addr);
        $boundary = country_boundary($pre_addr);
        $pre_addr = clean_postalcode($pre_addr, $boundary);

        # formatted_addr is a new builded address using libpostal api
        ($formatted_addr, $formatted_no_postal) = format_addr($pre_addr, $boundary);
        # If formatted_addr is equal to empty we proceed to send the raw address 
        # to the country_boundary function otherwise the formatted address is used.
 
        if($formatted_addr ne '') {
            $query{'text'} = $formatted_addr;
        } else {
            $query{'text'} = $pre_addr;
        }
        
        if ($boundary) {
            $query{'boundary.country'} = $boundary;
        }

        if($formatted_no_postal ne '') {
            if($parsed_addr{'postcode'} && $boundary ~~ @ctry_postcode) {
                $parsed{'postalcode'} = $parsed_addr{'postcode'};
                $parsed{'country'} = $boundary;

                $uri_pelias_structured->query_form(\%parsed);
                $response = $ua->get( $uri_pelias_structured );
                
                # check if we have a successful response
                if ($response->is_success) {
                    # get the first feature
                    $feature = $response->json_content->{features}[0];
                        
                    if($feature){

                        if($feature->{'properties'}->{'locality'} eq '' &&
                           $feature->{'properties'}->{'localadmin'} eq '' &&
                           $feature->{'properties'}->{'neighbourhood'} eq ''){
                                $check_response = 0;
                                $call_pelias = 0;
                                if($parsed_addr{'city'}){
                                  $parsed_addr{'city'} =~ s/([\w']+)/\u\L$1/g;
                                  $feature->{'properties'}->{'locality'} = $parsed_addr{'city'};
                                } elsif ($feature->{'properties'}->{'county'}){
                                  $feature->{'properties'}->{'locality'} = $feature->{'properties'}->{'county'};
                                } elsif ($parsed_addr{'county'}){
                                  $parsed_addr{'county'} =~ s/([\w']+)/\u\L$1/g;
                                  $feature->{'properties'}->{'locality'} = $parsed_addr{'county'};
                                }
                        }
                    }

                    if(not defined $feature->{'geometry'}->{'coordinates'}){
                        $check_response = 1;
                        $query{'text'} = $formatted_no_postal;
                        $uri_pelias->query_form(\%query);
                        # query the 'text' endpoint
                        $response = $ua->get($uri_pelias);
                        $call_layers = 1;
                    } elsif (($feature->{'geometry'}->{'coordinates'}[0] eq '0' &&
                        $feature->{'geometry'}->{'coordinates'}[1] eq '0') ||
                        $feature->{'properties'}->{'confidence'} < $threshold_confidence) {
                        $check_response = 1;
                        $query{'text'} = $formatted_no_postal;
                        $uri_pelias->query_form(\%query);
                        # query the 'text' endpoint
                        $response = $ua->get($uri_pelias);
                        $call_layers = 1;
                    }
                } else {
                    warning("%s %s", $response->code, $uri_pelias->path_query);
                }
            } elsif($boundary) {
                $query{'text'} = $formatted_no_postal;
                $query{'layers'} = 'locality,localadmin,macrocounty,county,macrohood,borough,neighbourhood,microhood,disputed';
                $uri_pelias->query_form(\%query);
                # query the 'text' endpoint
                $response = $ua->get($uri_pelias);
                delete $query{'layers'};
            }

            if($call_layers){
                if ($response->is_success) {                                                                                                                                                                                                                                                                        
                  # get the first feature                                                                                                                                   
                   $feature = $response->json_content->{features}[0];                                                                                                       
                   if(not defined $feature->{'geometry'}->{'coordinates'}){                                                                                                 
                      $query{'layers'} = 'locality,localadmin,macrocounty,county,macrohood,borough,neighbourhood,microhood,disputed';                            
                      $uri_pelias->query_form(\%query);                                                                                                                     
                      # query the 'text' endpoint                                                                                                                           
                      $response = $ua->get($uri_pelias);                                                                                                                    
                      delete $query{'layers'};                                                                                                                              
                    } elsif (($feature->{'geometry'}->{'coordinates'}[0] eq '0' &&                                                                                          
                        $feature->{'geometry'}->{'coordinates'}[1] eq '0') ||                                                                                               
                        ($layer_hierarchy{$feature->{'properties'}->{'layer'}} <                                                                                            
                        $layer_hierarchy{$config->threshold_layer}) ||                                                                                                      
                        $feature->{'properties'}->{'confidence'} < $threshold_confidence) {                                                                                 
                        $query{'layers'} = 'locality,localadmin,macrocounty,county,macrohood,borough,neighbourhood,microhood,disputed,postalcode';                          
                        $uri_pelias->query_form(\%query);
                          # query the 'text' endpoint                                                                                                                     
                        $response = $ua->get($uri_pelias);
                        delete $query{'layers'};
                      }                                                                                                                                                       
                  } else {                                                                                                                                                    
                        warning("%s %s", $response->code, $uri_pelias->path_query);                                                                                           
                  }                                                                                                                                                           
              } elsif($check_response) {
                    $query{'text'} = $formatted_no_postal;
                    # check if we have a successful response
                    if($response){
                      if ($response->is_success) {
                        # get the first feature
                         $feature = $response->json_content->{features}[0]; 
                         if(not defined $feature->{'geometry'}->{'coordinates'}){
                            $uri_pelias->query_form(\%query);
                            # query the 'text' endpoint
                            $response = $ua->get($uri_pelias);
                          } elsif (($feature->{'geometry'}->{'coordinates'}[0] eq '0' &&
                              $feature->{'geometry'}->{'coordinates'}[1] eq '0') || 
                              ($layer_hierarchy{$feature->{'properties'}->{'layer'}} < 
                              $layer_hierarchy{$config->threshold_layer}) ||
                              $feature->{'properties'}->{'confidence'} < $threshold_confidence) {
                              $uri_pelias->query_form(\%query);
                              # query the 'text' endpoint
                              $response = $ua->get($uri_pelias);
                          }
                      } else {
                            warning("%s %s", $response->code, $uri_pelias->path_query);
                      }
                    } else { 
                        $uri_pelias->query_form(\%query);
                        # query the 'text' endpoint
                        $response = $ua->get($uri_pelias);
                  }
              }
        }    
                                                                                                                                                                                         
        # check if we have a successful response
        if($response && $check_response){
            $query{'text'} = $pre_addr;
          if ($response->is_success) {
            # get the first feature
             $feature = $response->json_content->{features}[0]; 
             if(not defined $feature->{'geometry'}->{'coordinates'}){
                $uri_pelias->query_form(\%query);
                # query the 'text' endpoint
                $response = $ua->get($uri_pelias);
              } elsif (($feature->{'geometry'}->{'coordinates'}[0] eq '0' &&
                  $feature->{'geometry'}->{'coordinates'}[1] eq '0') || 
 				  ($layer_hierarchy{$feature->{'properties'}->{'layer'}} < 
                  $layer_hierarchy{$config->threshold_layer}) ||
				  $feature->{'properties'}->{'confidence'} < $threshold_confidence) {
                  $uri_pelias->query_form(\%query);
                  # query the 'text' endpoint
                  $response = $ua->get($uri_pelias);
              }
          } else {
                warning("%s %s", $response->code, $uri_pelias->path_query);
          }
        } 

        # check if we have a successful response
        if($response && $check_response){
          if ($response->is_success) {
             $call_pelias = 0;
	
             # get the first feature
             $feature = $response->json_content->{features}[0]; 
             if(not defined $feature->{'geometry'}->{'coordinates'}){ 
                  $call_pelias = 1;
             } elsif (($feature->{'geometry'}->{'coordinates'}[0] eq '0' &&
                  $feature->{'geometry'}->{'coordinates'}[1] eq '0') ||
 				  ($layer_hierarchy{$feature->{'properties'}->{'layer'}} < 
                  $layer_hierarchy{$config->threshold_layer}) ||
                  $feature->{'properties'}->{'confidence'} < $threshold_confidence) {
                  $call_pelias = 1;
            }
          } else {
                warning("%s %s", $response->code, $uri_pelias->path_query);
          }
        }
      }
      
      $query{'text'} = $address;

      if($call_pelias){ 
        # sets and returns query components that use the
        # application/x-www-form-urlencoded format
        $uri_pelias->query_form(\%query);
        my $response = $ua->get( $uri_pelias );        
        # check if we have a successful response
        if ($response->is_success) {
            # get the first feature
            $feature = $response->json_content->{features}[0];
        } else {
            warning("%s %s", $response->code, $uri_pelias->path_query);
        }
      }

      micro_feature_found:
      # if the the micro feature differs from the the macro/meso features,
      # use the macro/meso feature as a fallback feature
      if (($config->geocoding_mode ne 'vanilla' and 
          $config->geocoding_mode ne 'norm-code') and
          not is_micro_part_of_macro($feature,$macro_feature)) {

         # a null macro feature has (0,0) has cordinates
         my $null_macro_feature = (defined $macro_feature &&
                   scalar $macro_feature->{'geometry'}->{'coordinates'} &&
                         ($macro_feature->{'geometry'}->{'coordinates'}[0] == 0 &&
                          $macro_feature->{'geometry'}->{'coordinates'}[1] == 0)) || 0;
         # a weak macro feature has a confidence lower than 0.6
         # or (0,0) has cordinates
         my $weak_macro_feature = ((defined $macro_feature &&
                          $macro_feature->{'properties'}->{'confidence'}  < 0.6 ||
                          $null_macro_feature )) || 0;
         # try the macro-meso mode geocoding for weak features
         if (not $try_macro_meso and not $macro_meso_used and $weak_macro_feature) {
             $try_macro_meso = 1;
             goto no_macro_meso_used ;
         }
         # if the macro feature is not null
         if (defined $macro_feature && not $null_macro_feature) {
          $feature = $macro_feature;
         }
      } elsif (not defined $feature and defined $macro_feature) {
        $feature = $macro_feature;
      }

      calculate_nerf_distance:
      # a strong feature is an exact match with a confidence equal to 1
      my $strong_feature = ((defined $feature &&
                scalar $feature->{'properties'}->{'match_type'} &&
                       $feature->{'properties'}->{'match_type'} eq 'exact' &&
                       $feature->{'properties'}->{'confidence'} == 1)) || 0;

      # if we don't have a strong feature then recalculate the confidence
      if (($config->geocoding_mode ne 'vanilla' and 
           $config->geocoding_mode ne 'norm-code') and not $strong_feature) {
        
        # if we found a micro layer, then calculate the nerf distance
        # (normalize+expand+reduce+fingerprint distance) between the input
        # address and the retrieved one. The confidence is then recalculated as
        # 1 - nerf_distance and only results higher than the threshold are keep
        if (defined $feature and
            $layer_hierarchy{$feature->{'properties'}->{'layer'}} >=
            $layer_levels{'micro'}) {

           # micro names distance
           my $distance = nerf_distance($parsed{'address'},
                                        $feature->{'properties'}->{'name'},
                                        scalar $feature->{'properties'}->{'country_a'} ?
                                        lc($feature->{'properties'}->{'country_a'}) : undef,
                                        $config->geocoding_source,
                                        $Fingerprint::function{'bag_distance'}{'ref'});

           # new confidence value
           $feature->{'properties'}->{'confidence'}  = 1 - $distance;

           # only macro-micro results with a confidence higher or equal than
           # threshold are keep
           if (defined $macro_feature and
               $feature->{'properties'}->{'confidence'} <
               $threshold_confidence) {
             # use the macro feature as a fallback
             $feature = $macro_feature;
             # relax the threshold confidence in 90% and
             # comput again the nerf distance for the new feature
             $threshold_confidence = $threshold_confidence * 0.9;
             goto calculate_nerf_distance;
           }
        }  # micro layer

        # if we found a macro or meso layer, then calculate the nerf distance
        elsif (defined $feature and
            $layer_hierarchy{$feature->{'properties'}->{'layer'}} <
            $layer_levels{'micro'}) {
            my $distance = 0;
            my $oname = undef;
            my $iname = undef;
            my $layer = $feature->{'properties'}->{'layer'};
            my $hook_name = 'hook_' . $layer;

            if ($config->_exists($hook_name) and exists $parsed{$layer}) {
                $iname   = $parsed{$layer};
                my $hook = $config->get($hook_name);
                # get the value of the property
                if ($hook =~ /^sub/ || 0) {
                 $oname = (eval $hook)->();
                } else {
                 $oname =  eval $hook;
                }
            } else {
                $iname = $query{'text'};
                $oname = $feature->{'properties'}->{'label'};
            }

            # calculate the distance
            $distance =  nerf_distance($iname,
                           $oname,
                           scalar $feature->{'properties'}->{'country_a'} ?
                           lc($feature->{'properties'}->{'country_a'}) : undef,
                           $config->geocoding_source,
                           $Fingerprint::function{'bag_distance'}{'ref'});
            # new confidence value
            $feature->{'properties'}->{'confidence'}  = 1 - $distance;
        }  # macro or meso layer
      }  # if (not $strong_feature)
 
      print_result:
      # create output rows
      my $colref = ();

      # input rows
      foreach my $column_name (@input_columns) {
        push @{$colref}, $row->{$column_name};
      }

      # keep only results with a confidence higher or equal than a threshold
      if (defined $feature and
          defined $feature->{'properties'} and
          defined $feature->{'properties'}->{'confidence'} and
         $feature->{'properties'}->{'confidence'} < $threshold_confidence) {
        # remove all the features
        undef $feature;
        undef $macro_feature;
      }

      # keep only results with a layer smaller or equal than a threshold
      if (defined $feature and
          defined $feature->{'properties'} and
          defined $feature->{'properties'}->{'layer'} and
          $layer_hierarchy{$feature->{'properties'}->{'layer'}} <
          $layer_hierarchy{$config->threshold_layer}) {
         # remove all the features
        undef $feature;
        undef $macro_feature;
      }

      # if a confidence value was found
      if (defined $feature and
          defined $feature->{'properties'} and
          defined $feature->{'properties'}->{'confidence'}) {
        # increase the layer type (macro/meso/micro) count
        $stats{'layers'}{$layer_types{$feature->{'properties'}->{'layer'}}}++;
        # increase the iso3 count
        if ($feature->{'properties'}->{'country_a'}) {
          $stats{'countries'}{$feature->{'properties'}->{'country_a'}}{'count'}++;
          # increase the iso3 confidence
          $stats{'countries'}{$feature->{'properties'}->{'country_a'}}{'confidence'} +=
                              $feature->{'properties'}->{'confidence'};
        }
        # increase global confidence count
        $stats{'confidence'} += $feature->{'properties'}->{'confidence'};
        # confidence is formatted with two decimal places
        $feature->{'properties'}->{'confidence'} = sprintf("%.2f",
                          $feature->{'properties'}->{'confidence'});
      }

      # increase the number of non geocoded addresses
      if (not defined $feature) {
        $stats{'ungeocoded'}++;
      }

      # output rows
      my $column_value;
      foreach my $column_name (@service_columns) {
       if ($service_hook{$column_name}{'is_sub'}) {
        $column_value = (eval $service_hook{$column_name}{'hook'})->();
       } else {
        $column_value =  eval $service_hook{$column_name}{'hook'};
       }
        push @{$colref}, $column_value;
      }

      # combine columns into a string
      # address;label;longitude;latitude;confidence;accuracy;layer;locality;region;country
      $csv_out->print ($output_handler, $colref);

      # increase the number of imported addresses
      $count++;

      # only call update() again when needed
      if ($count >= $next_update) {
        $next_update = $progress->update($count);
      }
    }
    # try to close the input file
    close(INPUT)  or error("Could not close input file: '%s'", $file);
  }  # while (my $file = shift @ARGV)

  # number of addresses processed
  $benchmark{'global'}{'count'} = $count;

  # print benchmark results
  print_benchmark();
 
  # print stats results
  print_stats();
  
  # print summary of worker 
  print_summary();

  # ensure that the bar ends on 100%
  $progress->update($total_of_records) if $total_of_records > $next_update;

  # if we are writing on an output file
  if (not openhandle($config->output)) {
   # try to close the output file
   close($output_handler) or error("Could not close output file: '%s'", $config->output);

   # check if a we need to convert the csv to a database
   if ($config->database ne "") {
    create_database($total_of_records);
   }
  }
}

# =============================================================================
# Main program
# =============================================================================
setup_configuration();      # setup configuration environment
process_options();          # process options and command line arguments
init();                     # initialize
my $count = csv_count();    # count the number of records to process
print_params();
csv_worker($count);         # process csv files record by record until count
finish();                   # finish
exit(0);                    # exit with success
# =============================================================================

__END__

=head1 NAME

gaia-worker - bulk geocoding script

=head1 SYNOPSIS

B<gaia-worker> [I<OPTIONS>] FILES

=over

=item B<--geocoding_mode> macro-micro|I<macro-meso>|vanilla

=over 4

=item macro-meso: use only coarse features.

=item macro-micro: use the whole input address.

=item vanilla: no customizations are made.

=back

=item B<--output> FILE

File to write output to.

=item B<--count> N

Number of addresses to process.

=item B<--progress_bar> I<on>|off

Shows a progress bar.

=item B<--show-config>

Shows the current configuration.

=item B<--verbose>

Enable verbose mode.

=item B<--version>

Output version information and exit.

=item B<--help>

Display this help and exit.

=item B<--man>

Print the usual program information.

=back

See config.ini.example for more configuration options

=head1 AUTHOR

Cristian Martinez, E<lt>me@martinec.orgE<gt>

=head1 COPYRIGHT

Copyright (c) 2017, The CorTexT Platform.

This program is distributed under the Perl Artistic License 2.0.

=cut
