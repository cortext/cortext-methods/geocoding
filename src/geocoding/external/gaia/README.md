# Gaia Worker

> The [CorTexT Manager](https://docs.cortext.net/) is an online platform for digital humanities data
> analysis

## Overview

Gaia perl script allows bulk-geocoding addresses from a csv file using the
[Pelias](https://github.com/pelias/pelias) geocoder.

This script is part of the [CorTexT Geocoding
Method](https://gitlab.com/cortext/cortext-methods/geocoding). The method uses
[Pelias](https://github.com/pelias/pelias), a modular, free geocoder that uses the
[ElasticSearch](https://github.com/elastic/elasticsearch) engine and data from
[OpenStreetMap](https://www.openstreetmap.org/about).

## Dependencies

- Perl >= 5.24.1
- [Libpostal](https://github.com/openvenues/libpostal)
- CPAN dependencies specified in [cpanfile](./cpanfile)
- Local dependencies in `lib`

## Usage

```
perl gaia-worker.pl --output out.csv in.csv
```

## Options

You can set other options in the settings file, [config.ini](./config.ini).

---

[cortext]: https://managerv2.cortext.net
