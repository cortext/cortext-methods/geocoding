#!/usr/bin/env python
# -*- coding: utf-8 -*-

# cortext_geocoding - Cortext Geocoding Method
#
# Author (s):
# * Luis Daniel Medina Zuluaga <luis-daniel.medina-zuluaga@univ-eiffel.fr>
#
# License:
# [GNU-GPLv3+](https://www.gnu.org/licenses/gpl-3.0.html)
#
# Reference repository:
# <https://gitlab.com/cortext/cortext-methods/geocoding>
#
# Contributions are welcome, get in touch with the author(s).

#################################################
# Cortext Manager first calls this with python2 #
# and in that case we launch docker_wrapper     #
#################################################

from cortextlib.legacy import containerize  # noqa: F401

#################################
# Cortext Method Initialization #
#################################

from cortextlib.main import main

if __name__ == "__main__":
  main()
