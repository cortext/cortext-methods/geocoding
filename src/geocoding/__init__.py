# cortext_geocoding - Cortext Geocoding Method
#
# Author (s):
# * Luis Daniel Medina Zuluaga <luis-daniel.medina-zuluaga@univ-eiffel.fr>
#
# License:
# [GNU-GPLv3+](https://www.gnu.org/licenses/gpl-3.0.html)
#
# Reference repository:
# <https://gitlab.com/cortext/cortext-methods/geocoding>
#
# Contributions are welcome, get in touch with the author(s).

__author__ = "Luis Daniel Medina Zuluaga <luis-daniel.medina-zuluaga@univ-eiffel.fr>"
__copyright__ = "Copyright 2024-2024 Luis Daniel Medina Zuluaga"
__license__ = "GNU GPL version 3 or above"
__URL__ = "https://gitlab.com/cortext/cortext-methods/geocoding/"

__version__ = "0.0.1"


from .method import GeocodingMethod as Method